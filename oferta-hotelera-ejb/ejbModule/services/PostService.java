package services;

import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;

public class PostService {

	public static String sendPost(String destino, String json) {

		try {
			URL url = new URL(destino);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setDoOutput(true);
			urlConnection.setRequestMethod("POST");
			urlConnection.setRequestProperty("Content-Type", "text/plain");

			IOUtils.write(json, urlConnection.getOutputStream());
			if (urlConnection.getResponseCode() != 200) {
				throw new RuntimeException("Error de conexi�n: " + urlConnection.getResponseCode());
			}
			String response = IOUtils.toString(urlConnection.getInputStream());
			return response;

		} catch (Exception e) {
			return e.getMessage();
		}
	}

	public static String sendGet(String destino) {

		try {
			URL url = new URL(destino);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setDoOutput(true);
			urlConnection.setRequestMethod("GET");

			if (urlConnection.getResponseCode() != 200) {
				throw new RuntimeException("Error de conexi�n: " + urlConnection.getResponseCode());
			}
			String response = IOUtils.toString(urlConnection.getInputStream());
			return response;

		} catch (Exception e) {
			return e.getMessage();
		}
	}
}
