package services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class EndPoints {

	private static String BackOffice;
	private static String PortalWeb;
	private static String Propia;

	/**
	 * @return the backOffice
	 */
	public static String getBackOffice() {
		if (BackOffice == null || BackOffice == "")
			BackOffice = getPropValues("BackOffice");
		return BackOffice;
	}

	/**
	 * @return the propia
	 */
	public static String getPropia() {
		if (Propia == null || Propia == "")
			Propia = getPropValues("Propia");
		return Propia;
	}

	/**
	 * @return the portalWeb
	 */
	public static String getPortalWeb() {
		if (PortalWeb == null || PortalWeb == "")
			PortalWeb = getPropValues("PortalWeb");
		return PortalWeb;
	}

	/**
	 * @param backOffice
	 *            the backOffice to set
	 */
	public static Boolean setBackOffice(String backOffice) {
		if (setPropValues(backOffice, "BackOffice")) {
			BackOffice = backOffice;
			return true;
		} else
			BackOffice = "";
		return false;

	}

	/**
	 * @param portalWeb
	 *            the portalWeb to set
	 */
	public static Boolean setPortalWeb(String portalWeb) {
		if (setPropValues(portalWeb, "PortalWeb")) {
			PortalWeb = portalWeb;
			return true;
		} else
			PortalWeb = "";
		return false;
	}

	public static Boolean setPropia(String propia) {
		if (setPropValues(propia, "Propia")) {
			Propia = propia;
			return true;
		} else
			Propia = "";
		return false;
	}

	public static String getPropValues(String paramenter) {

		String result = "";

		InputStream inputStream = null;
		try {
			Properties prop = new Properties();
			String propFileName = "EndPoints.properties";

			File initialFile = new File(propFileName);
			inputStream = new FileInputStream(initialFile);

			prop.load(inputStream);
			// get the property value and print it out
			result = prop.getProperty(paramenter);

		} catch (Exception e) {
			System.out.println("Exception: " + e);
		} finally {
			try {
				inputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return result;
	}

	public static Boolean setPropValues(String paramenter, String propName) {

		Properties prop = new Properties();
		OutputStream output = null;
		InputStream inputStream = null;
		String propFileName = "EndPoints.properties";
		try {

			File initialFile = new File(propFileName);
			if (initialFile.exists() && !initialFile.isDirectory()) {
				inputStream = new FileInputStream(initialFile);
				prop.load(inputStream);
				inputStream.close();
			}

			output = new FileOutputStream(initialFile);

			// set the properties value
			prop.setProperty(propName, paramenter);

			// save properties to project root folder
			prop.store(output, null);

		} catch (Exception e) {
			System.out.println("Exception: " + e);
			return false;
		} finally {
			try {
				output.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

}
