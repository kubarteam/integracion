package vo;

import java.io.Serializable;
import java.util.Date;

public class LogVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private int id;
	private String modulo;	
	private String accion;
	private Date fecha;
	
	public LogVO(){}
	
	/**
	 * @param modulo the modulo to set
	 */
	public void setModulo(String modulo) {
		this.modulo = modulo;
	}

	/**
	 * @param accion the accion to set
	 */
	public void setAccion(String accion) {
		this.accion = accion;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public LogVO(int id, String modulo, String accion, Date fecha){
		this.id = id;
		this.modulo = modulo;
		this.accion = accion;
		this.fecha = fecha;
	}
	
	public LogVO(String modulo, String accion, Date fecha){
		this.modulo = modulo;
		this.accion = accion;
		this.fecha = fecha;
	}
	
	public int getId() {
		return id;
	}

	public String getModulo() {
		return modulo;
	}

	public String getAccion() {
		return accion;
	}

	public Date getFecha() {
		return this.fecha;
	}
	
	public String getStringFecha() {
		return this.fecha.toGMTString().substring(0, 11);
	}
}