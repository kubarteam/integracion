package com.mdb;

import java.util.Calendar;
import java.util.Properties;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;

import services.EndPoints;
import vo.LogVO;
public class MensajeriaBackOffice {

	public static void loguear(String Mensaje){
		Context context;
		try {
			
			String urlBack="http-remoting://"+EndPoints.getBackOffice()+":8080";
		    final Properties env = new Properties();
            env.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
            env.put(Context.PROVIDER_URL, System.getProperty(Context.PROVIDER_URL, urlBack));
//            env.put(Context.PROVIDER_URL, System.getProperty(Context.PROVIDER_URL, "http-remoting://192.168.0.101:8080"));
            env.put(Context.SECURITY_PRINCIPAL, System.getProperty("username", "backofficemensajeria"));
            env.put(Context.SECURITY_CREDENTIALS, System.getProperty("password", "mensajeria"));
            context = new InitialContext(env);
 
            String connectionFactoryString = System.getProperty("connection.factory", "jms/RemoteConnectionFactory");
            ConnectionFactory connectionFactory = (ConnectionFactory) context.lookup(connectionFactoryString);
 
            String destinationString = System.getProperty("destination", "java:/jms/queue/LogBackOffice");
            Destination destination = (Destination) context.lookup(destinationString);
 
            Connection connection = connectionFactory.createConnection(System.getProperty("username", "backofficemensajeria"), System.getProperty("password", "mensajeria"));
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            //consumer = session.createConsumer(destination);
            connection.start();
		
			MessageProducer producer = session.createProducer(destination);
			
			ObjectMessage message = session.createObjectMessage();
			LogVO pedido = new LogVO();
			pedido.setAccion(Mensaje);
			pedido.setModulo("Oferta Hotelera");
			pedido.setFecha(Calendar.getInstance().getTime());
			message.setObject(pedido);
			producer.send(message);
			// TODO: recordar cerrar la session y la connection en un bloque �finally�
			connection.close();
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
