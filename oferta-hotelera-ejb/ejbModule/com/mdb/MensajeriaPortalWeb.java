package com.mdb;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.hornetq.utils.json.JSONArray;
import org.hornetq.utils.json.JSONObject;

import com.model.Habitacion;
import com.model.Hotel;
import com.model.MediosPago;
import com.model.Oferta;
import com.model.Servicios;

import services.EndPoints;
public class MensajeriaPortalWeb {

	public static void NuevoHotel(Hotel hotel){
		Context context;
		try {

		    final Properties env = new Properties();
            env.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
		// reemplazar localhost por la IP
            env.put(Context.PROVIDER_URL, System.getProperty(Context.PROVIDER_URL, "http-remoting://"+EndPoints.getPortalWeb()+":8080"));
            env.put(Context.SECURITY_PRINCIPAL, System.getProperty("username", "backofficemensajeria"));
            env.put(Context.SECURITY_CREDENTIALS, System.getProperty("password", "mensajeria"));
            context = new InitialContext(env);
 
            String connectionFactoryString = System.getProperty("connection.factory", "jms/RemoteConnectionFactory");
            ConnectionFactory connectionFactory = (ConnectionFactory) context.lookup(connectionFactoryString);
 
            String destinationString = System.getProperty("destination", "java:/jms/queue/MensajesBackOffice");
            Destination destination = (Destination) context.lookup(destinationString);
 
            Connection connection = connectionFactory.createConnection(
            		System.getProperty("username", "backofficemensajeria"), 
            		System.getProperty("password", "mensajeria"));
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            connection.start();
			MessageProducer producer = session.createProducer(destination);
			

			String mensaje;
			JSONObject json = new JSONObject();
			json.put("ID_Hotel", "G10_"+String.format("%08d", hotel.getId()));
			json.put("Nombre", hotel.getNombre());
			json.put("Destino", hotel.getDestino());
			json.put("Direccion", hotel.getDestino());
			json.put("Estado", hotel.getEstado());
			json.put("Descripcion", hotel.getDescripcion());;
			json.put("FotoHotel", "http://"+EndPoints.getPropia()+":8080/oferta-hotelera-web"+hotel.getUrlFoto());
			json.put("FotoMapa", "http://"+EndPoints.getPropia()+":8080/oferta-hotelera-web"+ hotel.getUrlUbicacion());
			json.put("PoliticaCancelacion", hotel.getPoliticasCancelacion());
			
			JSONArray arrayServ = new JSONArray();				
			for(Servicios serv: hotel.getServicios()){
				arrayServ.put(serv.getServicio());
			}
			json.put("Servicios", arrayServ);
			
			
			JSONArray arrayHabitaciones = new JSONArray();
			for(Habitacion hab : hotel.getHabitaciones()){			
				JSONObject item = new JSONObject();
				item.put("ID_Habitacion", "test");
				item.put("Descripcion", 3);
				item.put("URLFoto", "course1");
				item.put("TipoHabitacion", "course1");
				
				JSONArray array = new JSONArray();				
				for(Servicios serv: hab.getServicios()){
					array.put(serv.getServicio());
				}
				item.put("Servicios", array);				
				
				arrayHabitaciones.put(item);
			}
			json.put("Habitaciones", arrayHabitaciones);

			JSONArray arrayMedios = new JSONArray();				
			for(MediosPago mp: hotel.getMedioPago()){
				arrayMedios.put(mp.getNombre());
			}
			json.put("MediosDePago", arrayMedios);
			
			
			mensaje = json.toString();
			
			ObjectMessage message = session.createObjectMessage();
		    message.setObject(mensaje);
		    producer.send(message);
			
			
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static void NuevaOferta(Oferta oferta){
		Context context;
		try {

		    final Properties env = new Properties();
            env.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
		// reemplazar localhost por la IP
            env.put(Context.PROVIDER_URL, System.getProperty(Context.PROVIDER_URL, "http-remoting://"+EndPoints.getPortalWeb()+":8080"));
            env.put(Context.SECURITY_PRINCIPAL, System.getProperty("username", "backofficemensajeria"));
            env.put(Context.SECURITY_CREDENTIALS, System.getProperty("password", "mensajeria"));
            context = new InitialContext(env);
 
            String connectionFactoryString = System.getProperty("connection.factory", "jms/RemoteConnectionFactory");
            ConnectionFactory connectionFactory = (ConnectionFactory) context.lookup(connectionFactoryString);
 
            String destinationString = System.getProperty("destination", "java:/jms/queue/MensajesBackOffice");
            Destination destination = (Destination) context.lookup(destinationString);
 
            Connection connection = connectionFactory.createConnection(
            		System.getProperty("username", "backofficemensajeria"), 
            		System.getProperty("password", "mensajeria"));
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            connection.start();
			MessageProducer producer = session.createProducer(destination);
			
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			String mensaje;
			JSONObject json = new JSONObject();
			json.put("ID_Hotel", "G10_"+String.format("%08d", oferta.getHotel().getId()));
			json.put("ID_Habitacion", "G10_"+String.format("%08d", oferta.getHabitacion().getId()));
			json.put("ID_Oferta", "G10_"+String.format("%08d", oferta.getId()));
			json.put("Desde",formatter.format(oferta.getDesde()));
			json.put("Hasta",formatter.format(oferta.getHasta()));
			json.put("Estado", oferta.getEstado());;
			json.put("CupoDiario", oferta.getCupo());
			json.put("Precio", oferta.getPrecio());
			
			
			mensaje = json.toString();
			
			ObjectMessage message = session.createObjectMessage();
		    message.setObject(mensaje);
		    producer.send(message);
			
			
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
