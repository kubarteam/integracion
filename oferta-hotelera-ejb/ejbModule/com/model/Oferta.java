package com.model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.dto.OfertaDTO;
import com.ejb.HotelBean;

@Entity
public class Oferta implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int id;
	public String titulo;
	public String estado;
	@ManyToOne
	public Hotel hotel;
	public Date desde;
	public Date hasta;
	public Float precio;
	@ManyToOne
	public Habitacion habitacion;
	public int cupo;

	
	public Oferta() {}
	public Oferta(OfertaDTO of) {
		HotelBean HB = new HotelBean();
		this.setId(of.getId());		
		this.setTitulo(of.getTitulo());
		this.setEstado(of.getEstado());
		this.setHotel(new Hotel(of.getHotel()));
		this.setHabitacion(new Habitacion(of.getHabitacion()));
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		
		try {
			this.setDesde(formatter.parse(of.getDesde()));
			this.setHasta(formatter.parse(of.getHasta()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.setPrecio(of.getPrecio());
		this.setCupo(of.getCupo());
		
	}
	
	
	public OfertaDTO toDTO(Boolean incluirHotel){
		OfertaDTO retorno = new OfertaDTO();
		retorno.setId(this.getId());		
		retorno.setTitulo(this.getTitulo());
		retorno.setEstado(this.getEstado());
		retorno.setIdHotel(this.getHotel().getId());
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		retorno.setDesde(formatter.format(this.getDesde()));
		retorno.setHasta(formatter.format(this.getHasta()));
		retorno.setPrecio(this.getPrecio());
		retorno.setIdHabitacion(this.getHabitacion().getId());
		retorno.setCupo(this.getCupo());
		if(incluirHotel){
			retorno.setHotel(this.getHotel().toDTO());
		}
		
		return retorno;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public Date getDesde() {
		return desde;
	}

	public void setDesde(Date desde) {
		this.desde = desde;
	}

	public Date getHasta() {
		return hasta;
	}

	public void setHasta(Date hasta) {
		this.hasta = hasta;
	}

	public Float getPrecio() {
		return precio;
	}

	public void setPrecio(Float precio) {
		this.precio = precio;
	}

	public Habitacion getHabitacion() {
		return habitacion;
	}

	public void setHabitacion(Habitacion habitacion) {
		this.habitacion = habitacion;
	}

	/**
	 * @return the cupo
	 */
	public int getCupo() {
		return cupo;
	}

	/**
	 * @param cupo
	 *            the cupo to set
	 */
	public void setCupo(int cupo) {
		this.cupo = cupo;
	}

}
