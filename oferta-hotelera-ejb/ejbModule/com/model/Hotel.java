package com.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.dto.HabitacionDTO;
import com.dto.HotelDTO;
import com.dto.MediosPagoDTO;
import com.dto.OfertaDTO;
import com.dto.ServiciosDTO;
import com.ejb.HotelBean;
import com.ejb.ServiciosBean;

@Entity
public class Hotel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int id;
	public String nombre;
	public String direccion; 
	public String estado;
	public String urlFoto;
	public String urlUbicacion;
	public String descripcion;
	
	@ManyToOne(cascade= CascadeType.ALL)
	public Solicitud solicitud;
	@ManyToMany
	public List<Servicios> servicios;
	/**
	 * @return the servicios
	 */
	public List<Servicios> getServicios() {
		return servicios;
	}


	/**
	 * @param servicios the servicios to set
	 */
	public void setServicios(List<Servicios> servicios) {
		this.servicios = servicios;
	}
	@OneToMany(cascade=CascadeType.ALL, mappedBy="hotel")
	
	public List<Habitacion> habitaciones;
	public String politicasCancelacion;
	
	@OneToMany(mappedBy="hotel")
	public List<Oferta> ofertas;
	@ManyToMany
	public List<MediosPago> medioPago;
	

	public Hotel() {}
	public Hotel(HotelDTO hot){
		
		
		this.setId(hot.getId());
		this.setDescripcion(hot.getDescripcion());
		this.setNombre(hot.getNombre());
		this.setDireccion(hot.getDireccion());
		this.setEstado(hot.getEstado());
		this.setPoliticasCancelacion(hot.getPoliticasCancelacion());
		this.setUrlFoto(hot.getUrlFoto());
		this.setUrlUbicacion(hot.getUrlUbicacion());
		this.setDestino(hot.getDestino());
		
		
		List<MediosPago> mp = new ArrayList<MediosPago>();
		for(MediosPagoDTO medio : hot.getMediosPago()){
			mp.add(new MediosPago(medio));
		}
		this.setMedioPago(mp);
		
		List<Servicios> servis = new ArrayList<Servicios>();
		for(ServiciosDTO servicio : hot.getServicios()){
			servis.add(new Servicios(servicio));
		}
		this.setServicios(servis);
	}
	
	
	public HotelDTO toDTO(){
		HotelDTO retorno = new HotelDTO();
		retorno.setId(this.getId());
		retorno.setDescripcion(this.getDescripcion());
		retorno.setNombre(this.getNombre());
		retorno.setDireccion(this.getDireccion());
		retorno.setEstado(this.getEstado());
		retorno.setPoliticasCancelacion(this.getPoliticasCancelacion());
		retorno.setUrlFoto(this.getUrlFoto());
		retorno.setUrlUbicacion(this.getUrlUbicacion());
		retorno.setDestino(this.getDestino());
		
		int[] idsServicios = new int[this.getServicios().size()];
		List<ServiciosDTO> servDTO = new ArrayList<ServiciosDTO>();
		for(int e =0;e< this.getServicios().size(); e++){
			idsServicios[e]=this.getServicios().get(e).getId();
			servDTO.add(this.getServicios().get(e).toDTO());
		}
		retorno.setServicios(servDTO);
		retorno.setIdsServicios(idsServicios);
		
		int[] idsMediosPagos = new int[this.getMedioPago().size()];
		List<MediosPagoDTO> mpDTO = new ArrayList<MediosPagoDTO>();
		for(int e =0;e< this.getMedioPago().size(); e++){
			idsMediosPagos[e]=this.getMedioPago().get(e).getId();
			mpDTO.add(this.getMedioPago().get(e).toDTO());
		}
		retorno.setMediosPago(mpDTO);
		retorno.setIdsMedioPago(idsMediosPagos);
		
		List<HabitacionDTO> habitaciones = new ArrayList<HabitacionDTO>();
		for(Habitacion hab : this.getHabitaciones()){
			habitaciones.add(hab.toDTO(false));
		}
		retorno.setHabitaciones(habitaciones);
		
		List<OfertaDTO> ofertas = new ArrayList<OfertaDTO>();
		for(Oferta of : this.getOfertas()){
			ofertas.add(of.toDTO(false));
		}
		retorno.setOfertas(ofertas);
		
		return retorno;
		
		
		
	}
	
	
	

	/**
	 * @return the medioPago
	 */
	public List<MediosPago> getMedioPago() {
		return medioPago;
	}


	/**
	 * @param medioPago the medioPago to set
	 */
	public void setMedioPago(List<MediosPago> medioPago) {
		this.medioPago = medioPago;
	}


	public List<Oferta> getOfertas() {
		return ofertas;
	}
	public void setOfertas(List<Oferta> ofertas) {
		this.ofertas = ofertas;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getUrlFoto() {
		return urlFoto;
	}
	public void setUrlFoto(String urlFoto) {
		this.urlFoto = urlFoto;
	}
	public String getUrlUbicacion() {
		return urlUbicacion;
	}
	public void setUrlUbicacion(String urlUbicacion) {
		this.urlUbicacion = urlUbicacion;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Solicitud getSolicitud() {
		return solicitud;
	}
	public void setSolicitud(Solicitud solicitud) {
		this.solicitud = solicitud;
	}
	public List<Habitacion> getHabitaciones() {
		return habitaciones;
	}
	public void setHabitaciones(List<Habitacion> habitaciones) {
		this.habitaciones = habitaciones;
	}
	public String getPoliticasCancelacion() {
		return politicasCancelacion;
	}
	public void setPoliticasCancelacion(String politicasCancelacion) {
		this.politicasCancelacion = politicasCancelacion;
	}
	
	public String destino;
	/**
	 * @return the destino
	 */
	public String getDestino() {
		return destino;
	}


	/**
	 * @param destino the destino to set
	 */
	public void setDestino(String destino) {
		this.destino = destino;
	}
	
	
}
