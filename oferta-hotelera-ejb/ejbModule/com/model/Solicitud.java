package com.model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.dto.SolicitudDTO;

@Entity
public class Solicitud implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int id;
	public Date fecha;
	public String estado;
	
	public Solicitud() {}
	public Solicitud(SolicitudDTO sol) throws ParseException{
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		if(sol.getID_Solicitud().contains("G"))
			sol.setID_Solicitud(sol.getID_Solicitud().substring(4, 11));
		this.setId(Integer.parseInt( sol.getID_Solicitud()));
		this.setEstado(sol.getEstado_Solicitud());
		this.setFecha(formatter.parse(sol.getFecha()));
		
	}
	
public SolicitudDTO toDTO() {
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		SolicitudDTO retorno = new SolicitudDTO();
		retorno.setID_Solicitud("G10_"+String.format("%08d", this.getId()) );
		retorno.setEstado_Solicitud(this.getEstado());
		retorno.setFecha(formatter.format(this.getFecha()));
		return retorno;
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
	
	
	
}
