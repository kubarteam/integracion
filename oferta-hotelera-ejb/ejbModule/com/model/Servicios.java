package com.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.dto.ServiciosDTO;

@Entity
public class Servicios implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int id;
	public String servicio;
	public String tipo;
	
	public Servicios() {}
	public Servicios(ServiciosDTO serv){
		
		this.setId(serv.getId());
		this.setServicio(serv.getServicio());
		this.setTipo(serv.getTipo());
	}
	
	
	/**
	 * @return the servicio
	 */
	public String getServicio() {
		return servicio;
	}
	/**
	 * @param servicio the servicio to set
	 */
	public void setServicio(String servicio) {
		this.servicio = servicio;
	}
	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}
	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	
	public ServiciosDTO toDTO() {
		// TODO Auto-generated method stub
		ServiciosDTO retorno = new ServiciosDTO();
		
		retorno.setId(this.getId());
		retorno.setServicio(this.getServicio());
		retorno.setTipo(this.getTipo());
		return retorno;
	}
	
	
}
