package com.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.dto.HabitacionDTO;
import com.dto.ServiciosDTO;
import com.ejb.HotelBean;
import com.ejb.ServiciosBean;

@Entity
public class Habitacion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int id;
	public String urlFoto;
	public String descripcion;
	public String tipoHabitacion;
	@ManyToOne
	public Hotel hotel;
	@ManyToMany
	public List<Servicios> servicios;
	
	public Habitacion() {}
	public Habitacion(HabitacionDTO hab){
		
		this.setUrlFoto(hab.getUrlFoto());
		this.setId(hab.getId());
		this.setDescripcion(hab.getDescripcion());
		this.setTipoHabitacion(hab.getTipoHabitacion());
		
		List<Servicios> servis = new ArrayList<Servicios>();
		for(ServiciosDTO servicio : hab.getServicios()){
			servis.add(new Servicios(servicio));
		}
		this.setServicios(servis);		
		this.setHotel(new Hotel(hab.getHotel()));
		
		
	}
	
	public HabitacionDTO toDTO(Boolean incluirHotel){
		HabitacionDTO retorno = new HabitacionDTO();
		retorno.setDescripcion(this.getDescripcion());
		retorno.setId(this.getId());
		retorno.setIdHotel(this.getHotel().getId());
		retorno.setTipoHabitacion(this.getTipoHabitacion());
		retorno.setUrlFoto(this.getUrlFoto());
		int[] idsServicios = new int[this.getServicios().size()];
		List<ServiciosDTO> servDTO = new ArrayList<ServiciosDTO>();
		for(int e =0;e< this.getServicios().size(); e++){
			idsServicios[e]=this.getServicios().get(e).getId();
			servDTO.add(this.getServicios().get(e).toDTO());
		}
		retorno.setServicios(servDTO);
		retorno.setIdsServicios(idsServicios);
		if(incluirHotel)
		retorno.setHotel(this.getHotel().toDTO());
		return retorno;
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUrlFoto() {
		return urlFoto;
	}
	public void setUrlFoto(String urlFoto) {
		this.urlFoto = urlFoto;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getTipoHabitacion() {
		return tipoHabitacion;
	}
	public void setTipoHabitacion(String tipoHabitacion) {
		this.tipoHabitacion = tipoHabitacion;
	}
	public List<Servicios> getServicios() {
		return servicios;
	}
	public void setServicios(List<Servicios> servicios) {
		this.servicios = servicios;
	}


	/**
	 * @return the hotel
	 */
	public Hotel getHotel() {
		return hotel;
	}


	/**
	 * @param hotel the hotel to set
	 */
	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}
	
	
	
}
