package com.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.dto.MediosPagoDTO;

@Entity
public class MediosPago implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int id;
	public String nombre;
	
	
	public MediosPago() {}
	
	public MediosPago(MediosPagoDTO mp){
		this.setId(mp.getId());
		this.setNombre(mp.getNombre());
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public MediosPagoDTO toDTO() {
		// TODO Auto-generated method stub
		MediosPagoDTO retorno = new MediosPagoDTO();
		retorno.setId(this.getId());
		retorno.setNombre(this.getNombre());
		return retorno;
	}
	
	
}
