package com.ejb;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonWriter;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


import com.dto.HabitacionDTO;
import com.dto.HotelDTO;
import com.dto.MediosPagoDTO;
import com.dto.OfertaDTO;
import com.dto.ServiciosDTO;
import com.mdb.MensajeriaBackOffice;
import com.mdb.MensajeriaPortalWeb;
import com.model.Habitacion;
import com.model.Hotel;
import com.model.MediosPago;
import com.model.Oferta;
import com.model.Servicios;
import com.model.Solicitud;

import services.EndPoints;
import services.PostService;

/**
 * Session Bean implementation class CategoryBean
 */
@Stateless
@LocalBean
public class HotelBean implements HotelBeanLocal {

	@PersistenceContext(unitName = "OfertaHoteleraPU")
	private EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public HotelBean() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the entityManager
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * @param entityManager
	 *            the entityManager to set
	 */
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	public int NuevoHotel(HotelDTO hotel) {

		List<MediosPagoDTO> mp = new ArrayList<MediosPagoDTO>();
		for (int idMedio : hotel.getIdsMedioPago()) {
			mp.add(entityManager.find(MediosPago.class, idMedio).toDTO());
		}
		hotel.setMediosPago(mp);

		List<ServiciosDTO> servis = new ArrayList<ServiciosDTO>();
		for (int idServicio : hotel.getIdsServicios()) {
			servis.add(entityManager.find(Servicios.class, idServicio).toDTO());
		}
		hotel.setServicios(servis);

		Hotel hot = new Hotel(hotel);

		hot.setEstado("INACTIVO");
		Solicitud sol = new Solicitud();
		sol.setFecha(new Date());
		sol.setEstado("PENDIENTE");
		hot.setSolicitud(sol);

		Hotel nuevo = entityManager.merge(hot);
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

		String mensaje;

		JsonObject solicitudObject = Json.createObjectBuilder()
				.add("ID_Hotel", "G10_" + String.format("%08d", nuevo.getId())).add("Nombre", nuevo.getNombre())
				.add("Direccion", nuevo.getDireccion()).add("Estado_Solicitud", nuevo.getSolicitud().getEstado())
				.add("ID_Solicitud", "G10_" + String.format("%08d", nuevo.getSolicitud().getId()))
				.add("fecha", formatter.format(new Date()))
				.add("url", "http://"+EndPoints.getPropia()+":8080/oferta-hotelera-web/api/Hoteles/ResponseSolicitud").build();

		entityManager.flush();
		// JASON A STRING.
		StringWriter stringWriter = new StringWriter();
		JsonWriter writer = Json.createWriter(stringWriter);
		writer.writeObject(solicitudObject);
		writer.close();

		mensaje = stringWriter.getBuffer().toString();

		PostService.sendPost(
				"http://" + EndPoints.getBackOffice() + ":8080/BackOfficeJAXRS/rest/ofertaHotel/solicitudHotel",
				mensaje);

		return nuevo.getId();
	}

	@Override
	public int AgregarHabitacion(HabitacionDTO habitacion) {

		habitacion.setHotel(entityManager.find(Hotel.class, habitacion.getIdHotel()).toDTO());

		List<ServiciosDTO> servis = new ArrayList<ServiciosDTO>();
		for (int idServicio : habitacion.getIdsServicios()) {
			servis.add(entityManager.find(Servicios.class, idServicio).toDTO());
		}
		habitacion.setServicios(servis);

		Habitacion hab = new Habitacion(habitacion);
		entityManager.merge(hab);
		entityManager.flush();
		return hab.getId();
	}

	@Override
	public int NuevaOferta(OfertaDTO oferta) {
		
		oferta.setHabitacion(entityManager.find(Habitacion.class, oferta.getIdHabitacion()).toDTO(true));
		
		oferta.setHotel(entityManager.find(Hotel.class, oferta.getHabitacion().getHotel().getId()).toDTO());
		
		Oferta of = new Oferta(oferta);
		entityManager.persist(of);
		entityManager.flush();

		MensajeriaPortalWeb.NuevaOferta(of);
		MensajeriaBackOffice.loguear("Se ha dado de alta la Oferta id G10_" + String.format("%08d", of.getId()));
		return of.getId();
	}

	@Override
	public int HotelAprobado(int idSolicitud, String estado) {
		Hotel hotel = (Hotel) entityManager.createQuery("from Hotel h where h.solicitud.id=" + idSolicitud)
				.getSingleResult();
		if (estado.contains("APROB")) {
			hotel.setEstado("ACTIVO");
			hotel.getSolicitud().setEstado("APROBADA");
		} else {
			hotel.setEstado("INACTIVO");
			hotel.getSolicitud().setEstado("RECHAZADA");
		}
		Hotel nuevo = entityManager.merge(hotel);
		entityManager.flush();
		

		return 1;

	}

	@Override
	public List<HotelDTO> GetHoteles() {
		List<Hotel> hoteles = entityManager.createQuery("from Hotel").getResultList();
		List<HotelDTO> retorno = new ArrayList<HotelDTO>();
		for (Hotel h : hoteles) {
			retorno.add(h.toDTO());
		}
		return retorno;
	}
	
	@Override
	public HotelDTO GetHotel(int id) {
		HotelDTO hotel = entityManager.find(Hotel.class, id).toDTO();
		
		return hotel;
	}

	@Override
	public List<OfertaDTO> GetOfertas() {
		List<Oferta> ofertas = entityManager.createQuery("from Oferta").getResultList();
		List<OfertaDTO> retorno = new ArrayList<OfertaDTO>();
		for (Oferta h : ofertas) {
			retorno.add(h.toDTO(true));
		}
		return retorno;
	}

	@Override
	public List<OfertaDTO> GetOfertasHotel(int idHotel) {
		List<Oferta> ofertas = entityManager.createQuery("from Oferta a where a.hotel.id=" + idHotel).getResultList();
		List<OfertaDTO> retorno = new ArrayList<OfertaDTO>();
		for (Oferta h : ofertas) {
			retorno.add(h.toDTO(true));
		}
		return retorno;
	}

	
	public Habitacion GetHabitacion(int id) {
		return entityManager.find(Habitacion.class, id);
	}

	@Override
	public List<HabitacionDTO> GetHabitacionesHotel(int idHotel) {
		List<Habitacion> hab = entityManager.createQuery("from Habitacion a where a.hotel.id=" + idHotel).getResultList();
		List<HabitacionDTO> retorno = new ArrayList<HabitacionDTO>();
		for (Habitacion h : hab) {
			retorno.add(h.toDTO(true));
		}
		return retorno;
	}

	@Override
	public Boolean PublicarHotel(int id) {
		// TODO Auto-generated method stub		
		try{
		Hotel nuevo = entityManager.find(Hotel.class, id);
		MensajeriaPortalWeb.NuevoHotel(nuevo);
		MensajeriaBackOffice.loguear("Se ha publicado a portal web el hotel id G10_" + String.format("%08d", id));
		return true;
		}
		catch(Exception e){
			System.out.println("error al publicar el hotel: "+ e.getMessage());
			return false;
		}
		
	}

	@Override
	public List<HabitacionDTO> GetHabitaciones() {
		List<Habitacion> hab = entityManager.createQuery("from Habitacion").getResultList();
		List<HabitacionDTO> retorno = new ArrayList<HabitacionDTO>();
		for (Habitacion h : hab) {
			retorno.add(h.toDTO(true));
		}
		return retorno;
	}

}
