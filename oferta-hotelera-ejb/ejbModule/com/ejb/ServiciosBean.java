package com.ejb;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.dto.MediosPagoDTO;
import com.dto.ServiciosDTO;
import com.model.MediosPago;
import com.model.Servicios;

import services.EndPoints;
import services.PostService;
@Stateless
@LocalBean
public class ServiciosBean implements ServiciosBeanLocal {
	@PersistenceContext(unitName = "OfertaHoteleraPU")
	private EntityManager entityManager;
	
	
	@Override
	public int NuevosServicios(List<ServiciosDTO> servicios) {
		// TODO Auto-generated method stub
		for(ServiciosDTO serv:servicios){
			entityManager.merge(new Servicios(serv));
		}
		return 1;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ServiciosDTO> GetServicios() {
		// TODO Auto-generated method stub
		List<ServiciosDTO> retorno = new ArrayList<ServiciosDTO>();
		List<Servicios> servDB =entityManager.createQuery("from Servicios").getResultList();
		for(Servicios s : servDB){
			retorno.add(s.toDTO());
		}
		return retorno;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MediosPagoDTO> GetMediosPago() {
		List<MediosPagoDTO> retorno = new ArrayList<MediosPagoDTO>();
		List<MediosPago> mpDB =entityManager.createQuery("from MediosPago").getResultList();
		for(MediosPago mp : mpDB){
			retorno.add(mp.toDTO());
		}
		return retorno;
	}
	
	public Servicios GetServicio(int id){
		
		return entityManager.find(Servicios.class, id);
	}

	public MediosPago GetMedioPago(int id){		
		return entityManager.find(MediosPago.class, id);
	}

	@Override
	public Boolean CambiarIPBack(String ip) {
		return EndPoints.setBackOffice(ip);
		
	}

	@Override
	public Boolean CambiarIPPortal(String ip) {
	return	EndPoints.setPortalWeb(ip);
		
	}


	@Override
	public Boolean CambiarIPPropia(String ip) {
		return EndPoints.setPropia(ip);
		
	}
	
	@Override
	public int ImportarServicios() {
		try{
		String retorno = PostService.sendGet("http://"+EndPoints.getBackOffice()+":8080/BackOfficeJAXRS/rest/servicios/getServicios");
		//String retorno="[{\"id\":1,\"tipo\":\"Establecimiento\",\"servicio\":\"Piscina cubierta\"},{\"id\":2,\"tipo\":\"Establecimiento\",\"servicio\":\"Desayuno buffet\"},{\"id\":3,\"tipo\":\"Establecimiento\",\"servicio\":\"Wifi en todo el hotel\"}]";
		List<Servicios> servicios = new ArrayList<Servicios>();
		JsonReader reader = Json.createReader(new StringReader(retorno));
		JsonArray array = reader.readArray();
		for(int e=0 ; e<array.size(); e++){
			JsonObject jsonO=	array.getJsonObject(e);
			Servicios ser = new Servicios();
			ser.setId(jsonO.getInt("id"));
			ser.setTipo(jsonO.getString("tipo"));
			ser.setServicio(jsonO.getString("servicio"));
			entityManager.merge(ser);
		}
		reader.close();
		entityManager.flush();
		}
		catch(Exception e){
			System.out.println("Error importando servicios: "+ e.getMessage());
		}
		return 1;
	}
}
