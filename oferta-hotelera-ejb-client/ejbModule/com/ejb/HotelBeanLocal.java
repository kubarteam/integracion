package com.ejb;

import java.util.List;

import javax.ejb.Local;

import com.dto.HabitacionDTO;
import com.dto.HotelDTO;
import com.dto.OfertaDTO;

@Local
public interface HotelBeanLocal {

	public int NuevoHotel(HotelDTO hotel);
	
	public int AgregarHabitacion(HabitacionDTO habitacion);
	
	public int NuevaOferta(OfertaDTO oferta);
	
	public int HotelAprobado(int idHotel, String estado);
	
	public List<HotelDTO> GetHoteles();
	
	public HotelDTO GetHotel(int id);
	
	public List<OfertaDTO>  GetOfertas();
	
	public List<OfertaDTO> GetOfertasHotel(int idHotel);
	
	public List<HabitacionDTO> GetHabitacionesHotel(int idHotel);
	
	public Boolean PublicarHotel(int id);
	
	public List<HabitacionDTO> GetHabitaciones();
}
