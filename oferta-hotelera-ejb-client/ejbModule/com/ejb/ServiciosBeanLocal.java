package com.ejb;

import java.util.List;

import javax.ejb.Local;

import com.dto.MediosPagoDTO;
import com.dto.ServiciosDTO;

@Local
public interface ServiciosBeanLocal {

	public int NuevosServicios(List<ServiciosDTO> servicios);
	
	public int ImportarServicios();
	
	public List<ServiciosDTO> GetServicios();
	
	public List<MediosPagoDTO> GetMediosPago();
	
	public Boolean CambiarIPBack(String ip);
	public Boolean CambiarIPPortal(String ip);
	
	public Boolean CambiarIPPropia(String ip);
	
}
