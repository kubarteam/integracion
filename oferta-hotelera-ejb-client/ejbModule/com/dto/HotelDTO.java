package com.dto;


import java.io.Serializable;
import java.util.List;


public class HotelDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int id;
	public String nombre;
	public String direccion; 
	public String estado;
	public String urlFoto;
	public String urlUbicacion;
	public String descripcion;
	public String destino;
	public List<HabitacionDTO> habitaciones;
	public List<OfertaDTO> ofertas;
	public int[] idsServicios;
	List<ServiciosDTO> servicios;
	public String politicasCancelacion;
	public int[] idsMedioPago;
	public List<MediosPagoDTO> mediosPago;
	
	/**
	 * @return the ofertas
	 */
	public List<OfertaDTO> getOfertas() {
		return ofertas;
	}
	/**
	 * @param ofertas the ofertas to set
	 */
	public void setOfertas(List<OfertaDTO> ofertas) {
		this.ofertas = ofertas;
	}
	/**
	 * @return the habitaciones
	 */
	public List<HabitacionDTO> getHabitaciones() {
		return habitaciones;
	}
	/**
	 * @param habitaciones the habitaciones to set
	 */
	public void setHabitaciones(List<HabitacionDTO> habitaciones) {
		this.habitaciones = habitaciones;
	}
	/**
	 * @return the destino
	 */
	public String getDestino() {
		return destino;
	}
	/**
	 * @param destino the destino to set
	 */
	public void setDestino(String destino) {
		this.destino = destino;
	}
	
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}
	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}
	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/**
	 * @return the urlFoto
	 */
	public String getUrlFoto() {
		return urlFoto;
	}
	/**
	 * @param urlFoto the urlFoto to set
	 */
	public void setUrlFoto(String urlFoto) {
		this.urlFoto = urlFoto;
	}
	/**
	 * @return the urlUbicacion
	 */
	public String getUrlUbicacion() {
		return urlUbicacion;
	}
	/**
	 * @param urlUbicacion the urlUbicacion to set
	 */
	public void setUrlUbicacion(String urlUbicacion) {
		this.urlUbicacion = urlUbicacion;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	/**
	 * @return the idsServicios
	 */
	public int[] getIdsServicios() {
		return idsServicios;
	}
	/**
	 * @param idsServicios the idsServicios to set
	 */
	public void setIdsServicios(int[] idsServicios) {
		this.idsServicios = idsServicios;
	}
	/**
	 * @return the servicios
	 */
	public List<ServiciosDTO> getServicios() {
		return servicios;
	}
	/**
	 * @param servicios the servicios to set
	 */
	public void setServicios(List<ServiciosDTO> servicios) {
		this.servicios = servicios;
	}
	/**
	 * @return the politicasCancelacion
	 */
	public String getPoliticasCancelacion() {
		return politicasCancelacion;
	}
	/**
	 * @param politicasCancelacion the politicasCancelacion to set
	 */
	public void setPoliticasCancelacion(String politicasCancelacion) {
		this.politicasCancelacion = politicasCancelacion;
	}
	/**
	 * @return the idsMedioPago
	 */
	public int[] getIdsMedioPago() {
		return idsMedioPago;
	}
	/**
	 * @param idsMedioPago the idsMedioPago to set
	 */
	public void setIdsMedioPago(int[] idsMedioPago) {
		this.idsMedioPago = idsMedioPago;
	}
	/**
	 * @return the mediosPago
	 */
	public List<MediosPagoDTO> getMediosPago() {
		return mediosPago;
	}
	/**
	 * @param mediosPago the mediosPago to set
	 */
	public void setMediosPago(List<MediosPagoDTO> mediosPago) {
		this.mediosPago = mediosPago;
	}
	
	
	
	
	
}
