package com.dto;

import java.io.Serializable;

public class SolicitudDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String ID_Solicitud;
	public String Estado_Solicitud;
	public String fecha;
	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}
	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	/**
	 * @return the iD_Solicitud
	 */
	public String getID_Solicitud() {
		return ID_Solicitud;
	}
	/**
	 * @param iD_Solicitud the iD_Solicitud to set
	 */
	public void setID_Solicitud(String iD_Solicitud) {
		ID_Solicitud = iD_Solicitud;
	}
	/**
	 * @return the estado_Solicitud
	 */
	public String getEstado_Solicitud() {
		return Estado_Solicitud;
	}
	/**
	 * @param estado_Solicitud the estado_Solicitud to set
	 */
	public void setEstado_Solicitud(String estado_Solicitud) {
		Estado_Solicitud = estado_Solicitud;
	}
	
	
}
