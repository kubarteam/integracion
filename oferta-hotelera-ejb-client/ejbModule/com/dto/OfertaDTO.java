package com.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


public class OfertaDTO implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int id;
	public String titulo;
	public String estado;
	public int idHotel;
	public String desde;
	public String hasta;
	public Float precio;
	public int idHabitacion;
	public int cupo;
	public HotelDTO hotel;
	public HabitacionDTO habitacion;
	
	
	
	/**
	 * @return the hotel
	 */
	public HotelDTO getHotel() {
		return hotel;
	}
	/**
	 * @param hotel the hotel to set
	 */
	public void setHotel(HotelDTO hotel) {
		this.hotel = hotel;
	}
	/**
	 * @return the habitacion
	 */
	public HabitacionDTO getHabitacion() {
		return habitacion;
	}
	/**
	 * @param habitacion the habitacion to set
	 */
	public void setHabitacion(HabitacionDTO habitacion) {
		this.habitacion = habitacion;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the titulo
	 */
	public String getTitulo() {
		return titulo;
	}
	/**
	 * @param titulo the titulo to set
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}
	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/**
	 * @return the idHotel
	 */
	public int getIdHotel() {
		return idHotel;
	}
	/**
	 * @param idHotel the idHotel to set
	 */
	public void setIdHotel(int idHotel) {
		this.idHotel = idHotel;
	}
	/**
	 * @return the desde
	 */
	public String getDesde() {
		return desde;
	}
	/**
	 * @param desde the desde to set
	 */
	public void setDesde(String desde) {
		this.desde = desde;
	}
	/**
	 * @return the hasta
	 */
	public String getHasta() {
		return hasta;
	}
	/**
	 * @param hasta the hasta to set
	 */
	public void setHasta(String hasta) {
		this.hasta = hasta;
	}
	/**
	 * @return the precio
	 */
	public Float getPrecio() {
		return precio;
	}
	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(Float precio) {
		this.precio = precio;
	}
	/**
	 * @return the idHabitacion
	 */
	public int getIdHabitacion() {
		return idHabitacion;
	}
	/**
	 * @param idHabitacion the idHabitacion to set
	 */
	public void setIdHabitacion(int idHabitacion) {
		this.idHabitacion = idHabitacion;
	}
	/**
	 * @return the cupo
	 */
	public int getCupo() {
		return cupo;
	}
	/**
	 * @param cupo the cupo to set
	 */
	public void setCupo(int cupo) {
		this.cupo = cupo;
	}
	
	
	
}
