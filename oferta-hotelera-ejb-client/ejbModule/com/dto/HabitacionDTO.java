package com.dto;

import java.io.Serializable;
import java.util.List;
public class HabitacionDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int id;
	public String urlFoto;
	public String descripcion;
	public String tipoHabitacion;
	public int idHotel;
	public int[] idsServicios;
	List<ServiciosDTO> servicios;
	public HotelDTO hotel;
	
	
	
	/**
	 * @return the hotel
	 */
	public HotelDTO getHotel() {
		return hotel;
	}
	/**
	 * @param hotel the hotel to set
	 */
	public void setHotel(HotelDTO hotel) {
		this.hotel = hotel;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the urlFoto
	 */
	public String getUrlFoto() {
		return urlFoto;
	}
	/**
	 * @param urlFoto the urlFoto to set
	 */
	public void setUrlFoto(String urlFoto) {
		this.urlFoto = urlFoto;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 * @return the tipoHabitacion
	 */
	public String getTipoHabitacion() {
		return tipoHabitacion;
	}
	/**
	 * @param tipoHabitacion the tipoHabitacion to set
	 */
	public void setTipoHabitacion(String tipoHabitacion) {
		this.tipoHabitacion = tipoHabitacion;
	}
	/**
	 * @return the idHotel
	 */
	public int getIdHotel() {
		return idHotel;
	}
	/**
	 * @param idHotel the idHotel to set
	 */
	public void setIdHotel(int idHotel) {
		this.idHotel = idHotel;
	}
	/**
	 * @return the idsServicios
	 */
	public int[] getIdsServicios() {
		return idsServicios;
	}
	/**
	 * @param idsServicios the idsServicios to set
	 */
	public void setIdsServicios(int[] idsServicios) {
		this.idsServicios = idsServicios;
	}
	/**
	 * @return the servicios
	 */
	public List<ServiciosDTO> getServicios() {
		return servicios;
	}
	/**
	 * @param servicios the servicios to set
	 */
	public void setServicios(List<ServiciosDTO> servicios) {
		this.servicios = servicios;
	}
	
	
}
