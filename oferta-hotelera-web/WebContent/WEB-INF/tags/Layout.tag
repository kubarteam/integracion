<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@ attribute name="title" required="true" rtexprvalue="true"%>
<%@ attribute name="content" fragment="true"%>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="UTF-8">
<meta name="description" content="">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<title><%=title%></title>

<!-- STYLESHEETS -->
<style type="text/css">
[fuse-cloak], .fuse-cloak {
	display: none !important;
}
</style>

<!-- Icons.css -->
<link type="text/css" rel="stylesheet"
	href="/oferta-hotelera-web/assets/icons/fuse-icon-font/style.css">

<!-- Animate.css -->
<link type="text/css" rel="stylesheet"
	href="/oferta-hotelera-web/assets/vendor/animate.css/animate.min.css">

<!-- PNotify -->
<link type="text/css" rel="stylesheet"
	href="/oferta-hotelera-web/assets/vendor/pnotify/pnotify.custom.min.css">

<!-- Nvd3 - D3 Charts -->
<link type="text/css" rel="stylesheet"
	href="/oferta-hotelera-web/assets/vendor/nvd3/build/nv.d3.min.css" />

<!-- Perfect Scrollbar -->
<link type="text/css" rel="stylesheet"
	href="/oferta-hotelera-web/assets/vendor/perfect-scrollbar/css/perfect-scrollbar.min.css" />

<!-- Fuse Html -->
<link type="text/css" rel="stylesheet"
	href="/oferta-hotelera-web/assets/vendor/fuse-html/fuse-html.min.css" />

<!-- Main CSS -->
<link type="text/css" rel="stylesheet"
	href="/oferta-hotelera-web/assets/css/main.css">
	
	<!-- Validation -->
<link type="text/css" rel="stylesheet"
	href="/oferta-hotelera-web/assets/css/validationEngine.jquery.css">
<!-- / STYLESHEETS -->


<!-- SELECT2 -->
<link type="text/css" rel="stylesheet"
	href="/oferta-hotelera-web/assets/css/select2.min.css">
	
	
<!-- DatePicker -->
<link type="text/css" rel="stylesheet"
	href="/oferta-hotelera-web/assets/css/bootstrap-datepicker.css">
	


<!-- JAVASCRIPT -->

<!-- jQuery -->
<script type="text/javascript"
	src="/oferta-hotelera-web/assets/vendor/jquery/dist/jquery.min.js"></script>

<!-- Mobile Detect -->
<script type="text/javascript"
	src="/oferta-hotelera-web/assets/vendor/mobile-detect/mobile-detect.min.js"></script>

<!-- Perfect Scrollbar -->
<script type="text/javascript"
	src="/oferta-hotelera-web/assets/vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>

<!-- Popper.js -->
<script type="text/javascript"
	src="/oferta-hotelera-web/assets/vendor/popper.js/index.js"></script>

<!-- Bootstrap -->
<script type="text/javascript"
	src="/oferta-hotelera-web/assets/vendor/bootstrap/bootstrap.min.js"></script>

<!-- Nvd3 - D3 Charts -->
<script type="text/javascript"
	src="/oferta-hotelera-web/assets/vendor/d3/d3.min.js"></script>
<script type="text/javascript"
	src="/oferta-hotelera-web/assets/vendor/nvd3/build/nv.d3.min.js"></script>

<!-- Data tables -->
<script type="text/javascript"
	src="/oferta-hotelera-web/assets/vendor/datatables.net/js/jquery.dataTables.min.js"></script>

<script type="text/javascript"
	src="/oferta-hotelera-web/assets/vendor/datatables-responsive/js/dataTables.responsive.js"></script>

<!-- PNotify -->
<script type="text/javascript"
	src="/oferta-hotelera-web/assets/vendor/pnotify/pnotify.custom.min.js"></script>

<!-- Fuse Html -->
<script type="text/javascript"
	src="/oferta-hotelera-web/assets/vendor/fuse-html/fuse-html.min.js"></script>

<!-- Main JS -->
<script type="text/javascript"
	src="/oferta-hotelera-web/assets/js/main.js"></script>

<!-- Validation -->
<script type="text/javascript"
	src="/oferta-hotelera-web/assets/js/jquery.validationEngine.js"></script>
	<script type="text/javascript"
	src="/oferta-hotelera-web/assets/js/jquery.validationEngine-es.js"></script>

<!-- Select2 -->
<script type="text/javascript"
	src="/oferta-hotelera-web/assets/js/select2.full.min.js"></script>
	
	<!-- Bootstrap growl -->
<script type="text/javascript"
	src="/oferta-hotelera-web/assets/js/jquery.bootstrap-growl.js"></script>
	
	<!-- DatePicker -->
<script type="text/javascript"
	src="/oferta-hotelera-web/assets/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript"
	src="/oferta-hotelera-web/assets/js/bootstrap-datepicker.es.js"></script>
	
<!-- / JAVASCRIPT -->
</head>

<body
	class="layout layout-vertical layout-left-navigation layout-below-toolbar">




	<div id="wrapper">



		<aside id="aside" class="aside aside-left" data-fuse-bar="aside"
			data-fuse-bar-media-step="md" data-fuse-bar-position="left">
			<div class="aside-content-wrapper">

				<div class="aside-content bg-primary-500 text-auto">

					<div class="aside-toolbar">

						<div class="logo">
							<a href="/oferta-hotelera-web/Views/index.jsp"><span
								class="logo-icon">OH</span></a> <span class="logo-text">Oferta
								Hotelera</span>
						</div>

						<button id="toggle-fold-aside-button" type="button"
							class="btn btn-icon d-none d-lg-block"
							data-fuse-aside-toggle-fold>
							<i class="icon icon-backburger"></i>
						</button>

					</div>

					<ul class="nav flex-column custom-scrollbar" id="sidenav"
						data-children=".nav-item">




						<li class="subheader"><span>MENU</span></li>





						<li class="nav-item" role="tab" id="heading-hoteles"><a
							class="nav-link ripple with-arrow collapsed"
							data-toggle="collapse" data-target="#collapse-hoteles" href="#"
							aria-expanded="false" aria-controls="collapse-hoteles"> <i
								class="icon s-4 icon-hotel"></i> <span>Hoteles</span>
						</a>

							<ul id="collapse-hoteles" class="collapse" role="tabpanel"
								aria-labelledby="heading-hoteles" data-children=".nav-item">



								<li class="nav-item"><a class="nav-link ripple "
									href="/oferta-hotelera-web/Views/HotelLista"> <span>Listado</span>
								</a></li>
								<li class="nav-item"><a class="nav-link ripple "
									href="/oferta-hotelera-web/Views/NuevoHotel"> <span>Nuevo</span>
								</a></li>



							</ul></li>

						<li class="nav-item" role="tab" id="heading-ofertas"><a
							class="nav-link ripple with-arrow collapsed"
							data-toggle="collapse" data-target="#collapse-ofertas" href="#"
							aria-expanded="false" aria-controls="collapse-ofertas"> <i
								class="icon s-4 icon-wallet-giftcard"></i> <span>Ofertas</span>
						</a>

							<ul id="collapse-ofertas" class="collapse" role="tabpanel"
								aria-labelledby="heading-ofertas" data-children=".nav-item">



								<li class="nav-item"><a class="nav-link ripple " href="/oferta-hotelera-web/Views/OfertaLista">
										<span>Listado</span>
								</a></li>

								<li class="nav-item"><a class="nav-link ripple " href="/oferta-hotelera-web/Views/NuevaOferta">
										<span>Nueva</span>
								</a></li>



							</ul></li>



						<li class="subheader" style="margin-top=60px;"><span>CONFIGURACION</span></li>

						<li class="nav-item" id="configuracionIPs">

							<div class="form-group" style="margin: 20px;">
								<input class="form-control md-has-value ip" type="text"
									id="ipBackOffice" name="ipBackOffice"> <label
									for="ipBackOffice">IP BackOffice</label>
							</div>
							<div class="form-group" style="margin: 20px;">
								<input class="form-control md-has-value ip" type="text"
									id="ipPortalWeb" name="ipPortalWeb"> <label
									for="ipPortalWeb">IP PortalWeb</label>
							</div>
							<div class="form-group" style="margin: 20px;">
								<input class="form-control md-has-value ip" type="text"
									id="ipPropia" name="ipPropia"> <label
									for="ipPropia">IP Propia</label>
							</div>
						</li>


					</ul>
				</div>
			</div>
		</aside>





		<div class="content" style="width: 100%;">

			<jsp:invoke fragment="content"></jsp:invoke>
		</div>

		<script type="text/javascript">
if(localStorage.ipBackOffice != undefined)
	$("#ipBackOffice").val(localStorage.ipBackOffice);
if(localStorage.ipPortalWeb != undefined)
	$("#ipPortalWeb").val(localStorage.ipPortalWeb);
if(localStorage.ipPropia != undefined)
	$("#ipPropia").val(localStorage.ipPropia);
	
	



$("input.ip").keypress(function(event){
	$(this).css("color","inherit");
	if ( event.which == 13 ) {
		var ip =$(this).val();
	var element= $(this);
	   if($(this).attr("id")=="ipBackOffice"){
		   $.ajax({
			   url:"/oferta-hotelera-web/api/Servicios/CambiarIPBackOffice",
			   type:"POST",
			   data:JSON.stringify({ip:ip}),
			   contentType:"application/json",
			   success: function(data){
				   if(data=="OK"){
				   localStorage.ipBackOffice=ip;
							   element.css("color","grey");
						   }
			   }
					  
			 });	
	   }
	   else if($(this).attr("id")=="ipPortalWeb") {
		   $.ajax({
			   url:"/oferta-hotelera-web/api/Servicios/CambiarIPPortalWeb",
			   type:"POST",
			   data:JSON.stringify({ ip:ip}),
			   contentType:"application/json",
			   success: function(data){
				   if(data=="OK"){
					  localStorage.ipPortalWeb=ip;
							   element.css("color","grey");
				   }
				  						 
					  }
			 });
	   }
	   else{
		   $.ajax({
			   url:"/oferta-hotelera-web/api/Servicios/CambiarIPPropia",
			   type:"POST",
			   data:JSON.stringify({ ip:ip}),
			   contentType:"application/json",
			   success: function(data){
				   if(data=="OK"){
					  localStorage.ipPropia=ip;
							   element.css("color","grey");
						 
				   }
					  }
			 });
	   }
	}
});	
</script>
	</div>





</body>
</html>