<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="mt" tagdir="/WEB-INF/tags"%>
<%@ page import="java.util.List, com.dto.HotelDTO" %>
<%
List<HotelDTO> hoteles = (List<HotelDTO>)request.getAttribute("hoteles");
%>
<mt:Layout title="Hoteles">
	<jsp:attribute name="content">
       
            <div id="e-commerce-products"
			class="page-layout carded full-width">

    <div class="top-bg bg-secondary"></div>

    <!-- CONTENT -->
    <div class="page-content">

        <!-- HEADER -->
        <div
					class="header bg-secondary text-auto row no-gutters align-items-center justify-content-between">

            <!-- APP TITLE -->
            <div class="col-12 col-sm">

                <div class="logo row no-gutters align-items-start">
                    <div class="logo-icon mr-3 mt-1">
                        <i class="icon-cube-outline s-6"></i>
                    </div>
                    <div class="logo-text">
                        <div class="h4">Hoteles</div>
                        <div class="">Total hoteles: ${hoteles.size() }</div>
                    </div>
                </div>

            </div>
            <!-- / APP TITLE -->

            <!-- SEARCH 
            <div class="col search-wrapper px-2">

                <div class="input-group">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-icon">
                            <i class="icon icon-magnify"></i>
                        </button>
                    </span>
                    <input id="products-search-input" type="text"
								class="form-control" placeholder="Search" aria-label="Search" />
                </div>

            </div>
            SEARCH -->

            <div class="col-auto">
                <a href="/oferta-hotelera-web/Views/NuevoHotel"
							class="btn btn-secondary">Nuevo Hotel</a>
            </div>

        </div>
        <!-- / HEADER -->

        <div class="page-content-card">

            <table id="e-commerce-products-table"
						class="table dataTable" style="margin-bottom:50px;">

                <thead>

                    <tr>

                        <th>
                            <div class="table-header">
                                <span class="column-title">ID</span>
                            </div>
                        </th>
 <th>
                            <div class="table-header">
                                <span class="column-title">Foto</span>
                            </div>
                        </th>

                        <th>
                            <div class="table-header">
                                <span class="column-title">Nombre</span>
                            </div>
                        </th>

                        <th>
                            <div class="table-header">
                                <span class="column-title">Direccion</span>
                            </div>
                        </th>
<th>
                            <div class="table-header">
                                <span class="column-title">Habitaciones cargadas</span>
                            </div>
                        </th>
                        <th>
                            <div class="table-header">
                                <span class="column-title">Estado</span>
                            </div>
                        </th>

                       

                        <th>
                            <div class="table-header">
                                <span class="column-title">Acciones</span>
                            </div>
                        </th>

                    </tr>
                </thead>

                <tbody>

                    
<c:forEach items="${hoteles}" var="hotel">
                        <tr>
                            <td>${hotel.getId()}</td>
                            <td><img class="product-image"
									src="/oferta-hotelera-web${hotel.urlFoto}"></td>
                            <td>${hotel.getNombre()}</td>
                            <td>${hotel.getDireccion()}</td>
                            <td>${hotel.getHabitaciones().size()}</td>   
                            <td>${hotel.getEstado()}</td>                           
                            <td>
                                <a href="/oferta-hotelera-web/Views/DetalleHotel?id=${ hotel.getId()}"
										class="btn btn-icon verDetalles" >
                                    <i class="icon icon-magnify s-4"></i>
                                </a>
                               
                            
                            </td>
                        </tr>
 </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
      </div>
      
      
      
      
</jsp:attribute>
</mt:Layout>