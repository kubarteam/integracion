<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="mt" tagdir="/WEB-INF/tags"%>
<%@ page import="java.util.List, com.dto.ServiciosDTO , com.dto.HabitacionDTO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%List<HabitacionDTO> habitaciones = (List<HabitacionDTO>)request.getAttribute("habitaciones");%>
<mt:Layout title="Nueva Oferta">
	<jsp:attribute name="content">
       
       
            <div id="e-commerce-products"
			class="page-layout carded full-width">

    <div class="top-bg bg-secondary"></div>

    <!-- CONTENT -->
    <div class="page-content">

        <!-- HEADER -->
        <div
					class="header bg-secondary text-auto row no-gutters align-items-center justify-content-between">

            <!-- APP TITLE -->
            <div class="col-12 col-sm">

                <div class="logo row no-gutters align-items-start">
                    <div class="logo-icon mr-3 mt-1">
                        <i class="icon-cube-outline s-6"></i>
                    </div>
                    <div class="logo-text">
                        <div class="h4">Nueva oferta</div>	
                    </div>
                </div>

            </div>
          

            <div class="col-auto">
                <button type="button" class="btn btn-secondary"
							onclick="enviar()">Guardar</button>
            </div>

        </div>
        <!-- / HEADER -->

        <div class="page-content-card">
<div class="col-12 col-md-12" style="margin-bottom: 50px;">

            <div class="example">

                <div class="description">

                    <div class="description-text">
                        <h5>Datos</h5>
                    </div>

                   
                </div>
<form method="post" action="/oferta-hotelera-web/NuevaOferta"
								 onsubmit="return validar()">
                <div class="source-preview-wrapper">
								<div class="preview">
									<div class="preview-elements">
               <div class="row">
<div class="col-md-12">
                <div class="form-group">
                     <label for="idHabitacion">Hotel</label>
                        <select class="form-control validate[required]"
															id="idHabitacion" name="idHabitacion">
                            
                            <c:forEach items="${habitaciones}" var="habi">
                            
                             <option value="${habi.getId() }">${habi.getHotel().getNombre()} Habitacion ${habi.getTipoHabitacion()}</option>
                            </c:forEach>
                           
                        </select>
                </div>
               
                    </div>
                </div>
                <div class="row">
<div class="col-md-3">
              <div class="form-group">  
         <input class="form-control md-has-value validate[required] fecha" type="date"
															id="desde" name="desde">
                    <label for="desde">Desde</label>
                </div>
                </div>
					<div class="col-md-3">
					 <div class="form-group">  
         <input class="form-control md-has-value validate[required] fecha" type="date"
															id="hasta" name="hasta">
                    <label for="hasta">Hasta</label>
                </div>
       
                    </div>
                    <div class="col-md-3">
					 <div class="form-group">  
         <input class="form-control md-has-value validate[required]" type="number"
															id="cupo" name="cupo">
                    <label for="cupo">Cupo</label>
                </div>
       
                    </div>
                    <div class="col-md-3">
					 <div class="form-group">  
         <input class="form-control md-has-value validate[required]" type="number"
															id="precio" name="precio">
                    <label for="hasta">Precio</label>
                </div>
       
                    </div>
                </div>
                 
                </div>
								</div>
               
							</div>
							</form>

            </div>

        </div>
          
        </div>
    </div>
      </div>
       
 <script>
		function enviar() {

			$("form").submit();
		}
		function validar(){
			return $("form").validationEngine("validate");
		}
		$(function(){
			$("form").validationEngine({promptPosition : "topLeft"});
			
			$("#idHabitacion").select2({
				  placeholder: 'Select an option',
			});
			
		});
		
// 		$(".fecha").datepicker({
//             autoclose: true,
//             format: "dd/mm/yyyy",
//             language: "es",
//             orientation:"bottom"
//         });
		
	</script>
</jsp:attribute>
</mt:Layout>