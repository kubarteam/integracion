<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="mt" tagdir="/WEB-INF/tags"%>
<%@ page import="java.util.List, com.dto.ServiciosDTO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%List<ServiciosDTO> servicios = (List<ServiciosDTO>)request.getAttribute("servicios");%>
<mt:Layout title="Nuevo Hotel">
	<jsp:attribute name="content">
       
       
            <div id="e-commerce-products"
			class="page-layout carded full-width">

    <div class="top-bg bg-secondary"></div>

    <!-- CONTENT -->
    <div class="page-content">

        <!-- HEADER -->
        <div
					class="header bg-secondary text-auto row no-gutters align-items-center justify-content-between">

            <!-- APP TITLE -->
            <div class="col-12 col-sm">

                <div class="logo row no-gutters align-items-start">
                    <div class="logo-icon mr-3 mt-1">
                        <i class="icon-cube-outline s-6"></i>
                    </div>
                    <div class="logo-text">
                        <div class="h4">Nuevo hotel</div>	
                    </div>
                </div>

            </div>
          

            <div class="col-auto">
                <button type="button" class="btn btn-secondary"
							onclick="enviar()">Guardar</button>
            </div>

        </div>
        <!-- / HEADER -->

        <div class="page-content-card">
<div class="col-12 col-md-12" style="margin-bottom: 50px;">

            <div class="example">

                <div class="description">

                    <div class="description-text">
                        <h5>Datos</h5>
                    </div>

                   
                </div>
<form method="post" action="/oferta-hotelera-web/NuevoHotel"
								enctype="multipart/form-data" onsubmit="return validar()">
                <div class="source-preview-wrapper">
								<div class="preview">
									<div class="preview-elements">
               <div class="row">
<div class="col-md-6">
                <div class="form-group">
                    <input class="form-control md-has-value validate[required]" type="text"
															id="nombre" name="nombre">
                    <label for="nombre">Nombre</label>
                </div>
                </div>
					<div class="col-md-6">
					 <div class="form-group">
                    <textarea class="form-control md-has-value validate[required]"
													id="descripcion" name="descripcion" cols="" rows="5"></textarea>
                    <label for="descripcion">Descripcion</label>
                </div>
       
                    </div>
                </div>
                <div class="row">
<div class="col-md-6">
              <div class="form-group">  
         <input class="form-control md-has-value validate[required]" type="text"
															id="direccion" name="direccion">
                    <label for="direccion">Direccion</label>
                </div>
                </div>
					<div class="col-md-6">
					 <div class="form-group">  
         <input class="form-control md-has-value validate[required]" type="text"
															id="destino" name="destino">
                    <label for="destino">Destino</label>
                </div>
       
                    </div>
                </div>
                 
                
                 <div class="row">
<div class="col-md-6">
                <div class="form-group">
                     <label for="servicios">Servicios</label>
                        <select multiple class="form-control validate[required]"
															id="servicios" name="servicios">
                            
                            <c:forEach items="${servicios}" var="servicio">
                            
                             <option value="${servicio.getId() }">${servicio.getServicio() }</option>
                            </c:forEach>
                           
                        </select>
                </div>
                </div>
					<div class="col-md-6">
         <div class="form-group">  
       	<label>Medios de pago</label>
       	<div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox"
																class="form-check-input validate[groupRequired[payments]]" name="mediosPago" value="1">
                            <span
																class="checkbox-icon fuse-ripple-ready"></span>
                            <span>Tarjeta</span>
                        </label>
                    </div>
      				<div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox"
																class="form-check-input validate[groupRequired[payments]]" name="mediosPago" value="2">
                            <span
																class="checkbox-icon fuse-ripple-ready"></span>
                            <span>Efectivo</span>
                        </label>
                    </div>
                    
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox"
																class="form-check-input validate[groupRequired[payments]]" name="mediosPago" value="3">
                            <span
																class="checkbox-icon fuse-ripple-ready"></span>
                            <span>Cheque</span>
                        </label>
                    </div>
                     <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox"
																class="form-check-input validate[groupRequired[payments]]" name="mediosPago" value="4	">
                            <span
																class="checkbox-icon fuse-ripple-ready"></span>
                            <span>Transferencia</span>
                        </label>
                    </div>
                </div>
                    </div>
                </div>
                
                
                
                
<div class="form-group">
                    <textarea class="form-control md-has-value validate[required]"
													id="politicasCancelacion" name="politicasCancelacion"
													cols="" rows=""></textarea>
                    <label for="politicasCancelacion">Politicas de cancelacion</label>
                </div>
                  <div class="row">
                <div class="col-md-6" style="margin-bottom: 50px;">
                
                <label class="custom-file">
                        <input type="file" id="fotoHotel"
														name="fotoHotel" class="custom-file-input validate[required]" >
                        <span class="custom-file-control">Foto Hotel</span>
                    </label>
               
                    </div>
                     <div class="col-md-6">
                      <label class="custom-file">
                        <input type="file" id="fotoUbicacion"
														name="fotoUbicacion" class="custom-file-input validate[required]" required="">
                        <span class="custom-file-control">Ubicacion </span>
                    </label>
                    
                    </div>
</div>
                </div>
								</div>
               
							</div>
							</form>

            </div>

        </div>
          
        </div>
    </div>
      </div>
       
 <script>
		function enviar() {

			$("form").submit();
		}
		function validar(){
			return $("form").validationEngine("validate");
		}
		$(function(){
			$("form").validationEngine({promptPosition : "topLeft"});
			
			$("#servicios").select2({
				  placeholder: 'Select an option'
			});
			
		});
		
	</script>
</jsp:attribute>
</mt:Layout>