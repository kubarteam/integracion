<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="mt" tagdir="/WEB-INF/tags"%>
<%
	String idHotel = (String)request.getAttribute("idHotel");
String nombre = (String)request.getAttribute("nombreHotel");
%>
<mt:Layout title="Nueva Habitacion">
	<jsp:attribute name="content">
	
            <div id="e-commerce-products"
			class="page-layout carded full-width">

    <div class="top-bg bg-secondary"></div>

    <!-- CONTENT -->
    <div class="page-content">

        <!-- HEADER -->
        <div
					class="header bg-secondary text-auto row no-gutters align-items-center justify-content-between">

            <!-- APP TITLE -->
            <div class="col-12 col-sm">

                <div class="logo row no-gutters align-items-start">
                    <div class="logo-icon mr-3 mt-1">
                        <i class="icon-cube-outline s-6"></i>
                    </div>
                    <div class="logo-text">
                   <div class="h4">Nueva Habitacion para   </div>	<h3>${nombre.toString()}</h3>
                    </div>
                </div>

            </div>
          

            <div class="col-auto">
                <button type="button" class="btn btn-secondary"
							onclick="enviar()">Guardar</button>
            </div>

        </div>
        <!-- / HEADER -->

        <div class="page-content-card">
<div class="col-12 col-md-12" style="margin-bottom: 50px;">

            <div class="example">

                <div class="description">

                    <div class="description-text">
                        <h5>Datos</h5>
                    </div>

                   
                </div>
<form method="post" action="/oferta-hotelera-web/NuevaHabitacion"
								enctype="multipart/form-data" onsubmit="return validar()">
								<input type="hidden" value="${idHotel.toString() }" name="idHotel"/>
                <div class="source-preview-wrapper">
								<div class="preview">
									<div class="preview-elements">
               <div class="row">
<div class="col-md-6">
                <div class="form-group">
                            <label for="tipoHabitacion" class="col-form-label">Tipo</label>
                            <select id="tipoHabitacion" name="tipoHabitacion" class="form-control">
					<option value="SIMPLE">SIMPLE</option>
					<option value="DOBLE">DOBLE</option>
					<option value="TRIPLE">TRIPLE</option>
					<option value="CUADRUPLE">CUADRUPLE</option>
				</select>
                        </div>
                        <div class="form-group">
                     <label for="servicios">Servicios</label>
                        <select multiple class="form-control validate[required]"
															id="servicios" name="servicios">
                            
                            <c:forEach items="${servicios}" var="servicio">
                            
                             <option value="${servicio.getId() }">${servicio.getServicio() }</option>
                            </c:forEach>
                           
                        </select>
                </div>
                </div>
					<div class="col-md-6">
					 <div class="form-group">
                    <textarea class="form-control md-has-value validate[required]"
													id="descripcion" name="descripcion" cols="" rows="3"></textarea>
                    <label for="descripcion">Descripcion</label>
                </div>
        <label class="custom-file">
                        <input type="file" id="fotoHabitacion"
														name="fotoHabitacion" class="custom-file-input validate[required]" >
                        <span class="custom-file-control">Foto</span>
                    </label>
                    </div>
                </div>
                                 
                
                 <div class="row">
<div class="col-md-6">
                
               
					
                    </div>
                </div>
                
                </div>
								</div>
               
							</div>
							</form>

            </div>

        </div>
          
        </div>
    </div>
      </div>
       
 <script>
		function enviar() {

			$("form").submit();
		}
		function validar(){
			return $("form").validationEngine("validate");
		}
		$(function(){
			$("form").validationEngine({promptPosition : "topLeft"});
			
			$("#servicios").select2({
				  placeholder: 'Select an option'
			});
			
		});
		
	</script>
	
	
	</jsp:attribute>
</mt:Layout>