<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="mt" tagdir="/WEB-INF/tags"%>
<%@ page import="java.util.List, com.dto.HotelDTO, com.dto.OfertaDTO, com.dto.HabitacionDTO" %>
<%
List<OfertaDTO> ofertas = (List<OfertaDTO>)request.getAttribute("ofertas");
%>
<mt:Layout title="Ofertas">
	<jsp:attribute name="content">
	
	
            <div id="e-commerce-products"
			class="page-layout carded full-width">

    <div class="top-bg bg-secondary"></div>

    <!-- CONTENT -->
    <div class="page-content">

        <!-- HEADER -->
        <div
					class="header bg-secondary text-auto row no-gutters align-items-center justify-content-between">

            <!-- APP TITLE -->
            <div class="col-12 col-sm">

                <div class="logo row no-gutters align-items-start">
                    <div class="logo-icon mr-3 mt-1">
                        <i class="icon-cube-outline s-6"></i>
                    </div>
                    <div class="logo-text">
                        <div class="h4">Ofertas</div>
                        <div class="">Total ofertas: ${ofertas.size() }</div>
                    </div>
                </div>

            </div>
            <!-- / APP TITLE -->

            <!-- SEARCH 
            <div class="col search-wrapper px-2">

                <div class="input-group">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-icon">
                            <i class="icon icon-magnify"></i>
                        </button>
                    </span>
                    <input id="products-search-input" type="text"
								class="form-control" placeholder="Search" aria-label="Search" />
                </div>

            </div>
            SEARCH -->

            <div class="col-auto">
                <a href="/oferta-hotelera-web/Views/NuevaOferta"
							class="btn btn-secondary">Nueva Oferta</a>
            </div>

        </div>
        <!-- / HEADER -->

        <div class="page-content-card">

            <table id="e-commerce-products-table"
						class="table dataTable" style="margin-bottom:50px;">

                <thead>

                    <tr>

                        <th>
                            <div class="table-header">
                                <span class="column-title">ID</span>
                            </div>
                        </th>
 <th>
                            <div class="table-header">
                                <span class="column-title">Hotel</span>
                            </div>
                        </th>

                       

                        <th>
                            <div class="table-header">
                                <span class="column-title">Habitacion</span>
                            </div>
                        </th>

                        <th>
                            <div class="table-header">
                                <span class="column-title">Precio</span>
                            </div>
                        </th>
                        <th>
                            <div class="table-header">
                                <span class="column-title">Desde</span>
                            </div>
                        </th>
                        <th>
                            <div class="table-header">
                                <span class="column-title">Hasta</span>
                            </div>
                        </th>
                         <th>
                            <div class="table-header">
                                <span class="column-title">Cupo diario</span>
                            </div>
                        </th>


                    </tr>
                </thead>

                <tbody>

                    
<c:forEach items="${ofertas}" var="oferta">
                        <tr>
                            <td>${oferta.getId()}</td>
                            <td>${oferta.getHotel().getNombre()}</td>                            
                            <td>${oferta.getHabitacion().getTipoHabitacion()}</td>
                            <td>${oferta.getPrecio()}</td>
                            <td>${oferta.getDesde()}</td>
                            <td>${oferta.getHasta()}</td>
                            <td>${oferta.getCupo()}</td>
                                                   
                           
                        </tr>
 </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
      </div>
      
      
	
	
	
	
	</jsp:attribute>
	</mt:Layout>
	