<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="mt" tagdir="/WEB-INF/tags"%>
<%@ page import="java.util.List, com.dto.HotelDTO"%>
<%
	HotelDTO hotel = (HotelDTO) request.getAttribute("hotel");
%>
<mt:Layout title="Detalle">
	<jsp:attribute name="content">
	
<div class="page-layout carded right-sidebar">

    <div class="top-bg bg-primary"></div>

    <div class="page-content-wrapper">

        <!-- CONTENT -->
        <div class="page-content">

            <!-- HEADER -->
            <div class="header py-6 bg-primary text-auto">

                <div class="d-flex flex-row align-items-center">

                    <button type="button"
								class="sidebar-toggle-button btn btn-icon d-block d-lg-none mr-2 fuse-ripple-ready"
								data-fuse-bar-toggle="demo-sidebar">
                        <i class="icon icon-menu"></i>
                    </button>

                    <span class="h3">${hotel.getNombre() }</span>

                </div>
            </div>
            <!-- / HEADER -->

            <div class="page-content-card">

                <!-- CONTENT TOOLBAR -->
                <div class="toolbar p-6">Informacion</div>
                <!-- / CONTENT TOOLBAR -->

                <div class="p-6">
                    <!-- DEMO CONTENT -->
                    <div class="demo-content">

    <img src="/oferta-hotelera-web/${hotel.getUrlFoto() }"
									class="w-100 mb-4" alt="sunrise">

<!--     <h1>Early Sunrise</h1> -->

    <h4 class="secondary-text">Descripcion</h4>

    <p>
       ${hotel.getDescripcion() }
    </p>
    
    <div class="row">
    <div class="col-md-6">
    <h4 class="secondary-text">Servicios</h4>

   <ul>
      <c:forEach items="${hotel.getServicios()}" var="item">
         <li>${item.getServicio() }</li>
      </c:forEach>
      </ul>
    
    </div>
    <div class="col-md-6">
     <h4 class="secondary-text">Medios de pago</h4>

   <ul>
      <c:forEach items="${hotel.getMediosPago()}" var="item">
         <li>${item.getNombre() }</li>
      </c:forEach>
      </ul>
    
    </div>
    
    </div>
     <h4 class="secondary-text">Politicas de cancelacion</h4>

    <p>
       ${hotel.getPoliticasCancelacion() }
    </p>
    
   
</div>
                    <!-- / DEMO CONTENT -->
                </div>

            </div>
        

            <div class="page-content-card">

                <!-- CONTENT TOOLBAR -->
                <div class="toolbar p-6">Ubicacion</div>
                <!-- / CONTENT TOOLBAR -->

                <div class="p-6" style="padding: 1rem !important;">
                    <!-- DEMO CONTENT -->
                    <div class="demo-content">

    <img src="/oferta-hotelera-web/${hotel.getUrlUbicacion() }"
									class="w-100 mb-4" alt="sunrise">

  

    <h4 class="secondary-text">${hotel.getDireccion() }, ${hotel.getDestino() }</h4>

   
    
   
</div>
                    <!-- / DEMO CONTENT -->
                </div>

            </div>
            
            <div class="col-12">
            <h4 id="content-types">Habitaciones</h4>
        </div>
        <div class="row">
            <c:forEach items="${hotel.getHabitaciones()}" var="hab">
            <div class="col-12 col-sm-6 doc">
            <div class="example">
                <div class="description">
                    <div class="description-text">
                        <h5>Habitacion ${hab.getTipoHabitacion() }</h5>
                    </div>
                </div>                
				<div class="preview">
									<div class="preview-elements">
                <div class="card">
                    <div class="card-body">
                        <img src="/oferta-hotelera-web/${hab.getUrlFoto() }"
									class="w-100 mb-4" alt="sunrise">
									
						<p>${hab.getDescripcion() }</p>
						<br/>
						<h6>Servicios</h6>
						  <c:forEach items="${hab.getServicios()}" var="ser">
						  <p style="margin-left:10px;">${ser.getServicio() }</p>
						  </c:forEach>
                    </div>
                </div>
                </div>
             </div>
               
			</div>
            </div>
</c:forEach>
   
            </div>
            
            
            
            
            
            
        </div>
        <!-- / CONTENT -->
   




        <aside class="page-sidebar" data-fuse-bar="demo-sidebar"
					data-fuse-bar-media-step="md" data-fuse-bar-position="right">

            <!-- HEADER -->
            <div class="header p-6 bg-primary text-auto">
            	<label id="estadoLabel" style="margin-bottom: 0px;">Estado</label>
                <h4>${hotel.getEstado() }</h4>
            </div>
            <!-- / HEADER -->

            <!-- DEMO CONTENT -->
            <div class="demo-sidebar">

    <ul class="nav flex-column">

        <li class="subheader">Acciones</li>

        <li class="nav-item">
            <a class="nav-link" href="/oferta-hotelera-web/Views/NuevaHabitacion?idHotel=${hotel.getId()}&nombreHotel=${hotel.getNombre()}">Nueva Habitacion</a>
        </li>

  

        <li class="nav-item">
         <c:if test = "${!hotel.getEstado().equals(\"INACTIVO\")}">
          <a class="nav-link" id="publicarHotel" >Publicar</a>
      </c:if>
           
        </li>

        

    </ul>
</div>

            <!-- / DEMO CONTENT -->

        </aside>
    </div>
    </div>
       <script>
       $("#publicarHotel").click(function(){
    	   $.ajax({
    		   url:"/oferta-hotelera-web/api/Hoteles/PublicarHotel",
    		   type:"POST",
    		   data:{id:${hotel.getId()}},
    		   contentType: "application/json",
    		   success:function(data){
    		   
    		   if(data=="OK"){
    			   $("#estadoLabel").text("PUBLICADO");
    			   $.bootstrapGrowl("Hotel publicado con exito");
    		   }
    		   else{
    			   $.bootstrapGrowl("Ocurrio un error en la publicacion", {    				 
    				   type: 'danger', // (null, 'info', 'danger', 'success')    				  
    				 });
    		   }
    		   
    	  		 }
    		   });
    	   
       });
       </script>
	</jsp:attribute>
</mt:Layout>
