package com.api.controllers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Paths;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.dto.HotelDTO;
import com.ejb.HotelBeanLocal;

/**
 * Servlet implementation class NuevoHotel
 */
@WebServlet("/NuevoHotel")
@MultipartConfig
public class NuevoHotel extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private HotelBeanLocal hotelBeanLocal;
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public NuevoHotel() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HotelDTO hotel = new HotelDTO();

		hotel.setDescripcion(request.getParameter("descripcion"));
		hotel.setNombre(request.getParameter("nombre"));
		hotel.setPoliticasCancelacion(request.getParameter("politicasCancelacion"));
		hotel.setDestino(request.getParameter("destino"));
		hotel.setDireccion(request.getParameter("direccion"));

		String[] mediosIdsString=request.getParameterValues("mediosPago");
		int[] mediosIds = new int[mediosIdsString.length];
		for(int e=0; e< mediosIdsString.length;e++){
			mediosIds[e]= Integer.parseInt(mediosIdsString[e]);
		}		
		hotel.setIdsMedioPago(mediosIds);
		
		String[] serviciosIdsString=request.getParameterValues("servicios");
		int[] serviciosIds = new int[serviciosIdsString.length];
		for(int e=0; e< serviciosIdsString.length;e++){
			serviciosIds[e]= Integer.parseInt(serviciosIdsString[e]);
		}		
		hotel.setIdsServicios(serviciosIds);
		
		

		Part fotoHotel_Part = request.getPart("fotoHotel");
		String fotoHotel_name = Paths.get(fotoHotel_Part.getSubmittedFileName()).getFileName().toString();
		InputStream fotoHotel_content = fotoHotel_Part.getInputStream();		
		String fotoHotel_path = getServletContext().getRealPath("/FotosHoteles/"+fotoHotel_name);
		File fotoHotel_file = new File(fotoHotel_path);
		if(!fotoHotel_file.exists()){
			byte[] fotoHotel_buffer = new byte[fotoHotel_content.available()];
			fotoHotel_content.read(fotoHotel_buffer);
			OutputStream fotoHotel_output = new FileOutputStream(fotoHotel_file);
			fotoHotel_output.write(fotoHotel_buffer);
			fotoHotel_output.close();
		}
		hotel.setUrlFoto("/FotosHoteles/"+fotoHotel_name);
		
		Part fotoUbicacion_Part = request.getPart("fotoUbicacion");
		String fotoUbicacion_name = Paths.get(fotoUbicacion_Part.getSubmittedFileName()).getFileName().toString();
		InputStream fotoUbicacion_content = fotoUbicacion_Part.getInputStream();		
		String fotoUbicacion_path = getServletContext().getRealPath("/FotosHoteles/"+fotoUbicacion_name);
		File fotoUbicacion_file = new File(fotoUbicacion_path);
		if(!fotoUbicacion_file.exists()){
			byte[] fotoUbicacion_buffer = new byte[fotoUbicacion_content.available()];
			fotoUbicacion_content.read(fotoUbicacion_buffer);
			OutputStream fotoUbicacion_output = new FileOutputStream(fotoUbicacion_file);
			fotoUbicacion_output.write(fotoUbicacion_buffer);
			fotoUbicacion_output.close();
		}
		hotel.setUrlUbicacion("/FotosHoteles/"+fotoUbicacion_name);
		
		hotelBeanLocal.NuevoHotel(hotel);
		
		
		RequestDispatcher rd = request.getRequestDispatcher("/HotelLista");
		rd.forward(request, response);
	}

}
