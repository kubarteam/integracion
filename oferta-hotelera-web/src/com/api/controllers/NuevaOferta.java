package com.api.controllers;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dto.OfertaDTO;
import com.ejb.HotelBeanLocal;

/**
 * Servlet implementation class NuevaOferta
 */
@WebServlet("/NuevaOferta")
public class NuevaOferta extends HttpServlet {
	private static final long serialVersionUID = 1L;
       @EJB
       private HotelBeanLocal hotelBeanLocal;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NuevaOferta() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OfertaDTO oferta = new OfertaDTO();
		
		
		oferta.setIdHabitacion(Integer.parseInt(request.getParameter("idHabitacion")));
		oferta.setDesde(request.getParameter("desde"));
		oferta.setHasta(request.getParameter("hasta"));
		oferta.setCupo(Integer.parseInt(request.getParameter("cupo")));
		oferta.setPrecio(Float.parseFloat(request.getParameter("precio")));
		
		hotelBeanLocal.NuevaOferta(oferta);	
		RequestDispatcher rd = request.getRequestDispatcher("/Views/OfertaLista");
		rd.forward(request, response);
	}

}
