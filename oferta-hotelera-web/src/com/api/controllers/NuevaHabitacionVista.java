package com.api.controllers;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dto.HotelDTO;
import com.dto.ServiciosDTO;
import com.ejb.HotelBeanLocal;
import com.ejb.ServiciosBeanLocal;

/**
 * Servlet implementation class NuevaHabitacionVista
 */

@WebServlet("/Views/NuevaHabitacion")
public class NuevaHabitacionVista extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@EJB
	private HotelBeanLocal hotelBeanLocal;
	 @EJB
     private ServiciosBeanLocal serviciosBeanLocal;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NuevaHabitacionVista() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("idHotel");
		String nombre = request.getParameter("nombreHotel");
		request.setAttribute("idHotel",id);
		request.setAttribute("nombreHotel",nombre);
		
		serviciosBeanLocal.ImportarServicios();
		List<ServiciosDTO> servicios = serviciosBeanLocal.GetServicios();
		request.setAttribute("servicios", servicios);
		
		RequestDispatcher rd = request.getRequestDispatcher("/Views/NuevaHabitacion.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
