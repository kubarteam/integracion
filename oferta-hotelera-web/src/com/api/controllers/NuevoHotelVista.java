package com.api.controllers;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dto.ServiciosDTO;
import com.ejb.HotelBeanLocal;
import com.ejb.ServiciosBeanLocal;



/**
 * Servlet implementation class NuevoHotelVista
 */
@WebServlet("/Views/NuevoHotel")
public class NuevoHotelVista extends HttpServlet {
	private static final long serialVersionUID = 1L;
       @EJB
       private HotelBeanLocal hotelBeanLocal;
       @EJB
       private ServiciosBeanLocal serviciosBeanLocal;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NuevoHotelVista() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		serviciosBeanLocal.ImportarServicios();
		List<ServiciosDTO> servicios = serviciosBeanLocal.GetServicios();
		request.setAttribute("servicios", servicios);
		RequestDispatcher rd = request.getRequestDispatcher("/Views/NuevoHotel.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
