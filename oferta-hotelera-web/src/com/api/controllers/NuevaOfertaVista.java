package com.api.controllers;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dto.HabitacionDTO;
import com.dto.HotelDTO;
import com.dto.OfertaDTO;
import com.ejb.HotelBeanLocal;

/**
 * Servlet implementation class NuevaOfertaVista
 */
@WebServlet("/Views/NuevaOferta")
public class NuevaOfertaVista extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private HotelBeanLocal hotelBeanLocal;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public NuevaOfertaVista() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		List<HabitacionDTO> habitaciones = hotelBeanLocal.GetHabitaciones();

		request.setAttribute("habitaciones", habitaciones);
		RequestDispatcher rd = request.getRequestDispatcher("/Views/NuevaOferta.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
