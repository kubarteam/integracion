package com.api.controllers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Paths;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.dto.HabitacionDTO;
import com.dto.HotelDTO;
import com.ejb.HotelBeanLocal;

/**
 * Servlet implementation class NuevaHabitacion
 */
@WebServlet("/NuevaHabitacion")
@MultipartConfig
public class NuevaHabitacion extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private HotelBeanLocal hotelBeanLocal;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public NuevaHabitacion() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HabitacionDTO hab = new HabitacionDTO();

		hab.setDescripcion(request.getParameter("descripcion"));
		hab.setTipoHabitacion(request.getParameter("tipoHabitacion"));

		hab.setIdHotel(Integer.parseInt(request.getParameter("idHotel")));

		String[] serviciosIdsString = request.getParameterValues("servicios");
		int[] serviciosIds = new int[serviciosIdsString.length];
		for (int e = 0; e < serviciosIdsString.length; e++) {
			serviciosIds[e] = Integer.parseInt(serviciosIdsString[e]);
		}
		hab.setIdsServicios(serviciosIds);

		Part fotoHabitacion_Part = request.getPart("fotoHabitacion");
		String fotoHabitacion_name = Paths.get(fotoHabitacion_Part.getSubmittedFileName()).getFileName().toString();
		InputStream fotoHabitacion_content = fotoHabitacion_Part.getInputStream();
		String fotoHabitacion_path = getServletContext().getRealPath("/FotosHoteles/" + fotoHabitacion_name);
		File fotoHabitacion_file = new File(fotoHabitacion_path);
		if (!fotoHabitacion_file.exists()) {
			byte[] fotoHabitacion_buffer = new byte[fotoHabitacion_content.available()];
			fotoHabitacion_content.read(fotoHabitacion_buffer);
			OutputStream fotoHabitacion_output = new FileOutputStream(fotoHabitacion_file);
			fotoHabitacion_output.write(fotoHabitacion_buffer);
			fotoHabitacion_output.close();
		}
		hab.setUrlFoto("/FotosHoteles/" + fotoHabitacion_name);

		hotelBeanLocal.AgregarHabitacion(hab);

		request.setAttribute("id", request.getParameter("idHotel"));
		RequestDispatcher rd = request.getRequestDispatcher("/Views/DetalleHotel");
		rd.forward(request, response);
	}

}
