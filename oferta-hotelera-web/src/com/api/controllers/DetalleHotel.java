package com.api.controllers;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dto.HotelDTO;
import com.ejb.HotelBeanLocal;


/**
 * Servlet implementation class DetalleHotel
 */
@WebServlet("/Views/DetalleHotel")
@MultipartConfig
public class DetalleHotel extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@EJB
	private HotelBeanLocal hotelBeanLocal;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DetalleHotel() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		if (id==null)
			id = request.getParameter("idHotel");
		HotelDTO hotel = hotelBeanLocal.GetHotel(Integer.parseInt(id));
		
		request.setAttribute("hotel",hotel);
		
		RequestDispatcher rd = request.getRequestDispatcher("/Views/DetalleHotel.jsp");
		rd.forward(request, response);
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
