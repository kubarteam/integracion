/**
 * 
 */
package com.api.util;

/**
 * @author huicha
 *
 */
public class ConfiguracionG12 {

	private ClienteMensajeria client;

	public ConfiguracionG12(String json) {
		this.client = new ClienteMensajeria();

		String host = "10.0.0.100";
		String port = "8080";
//		String queueName = "jms/queue/solicitudArticulo2";
//		String queueName = "jms/queue/DespachoNuevoArticulo";
//		String queueName = "jms/queue/ColaSolicitudesArticulos";
		String queueName = "jms/queue/eventoAuditoria";
		String user = "guest";
		String pass = "guest";

		client.setProviderUrl(host + ":" + port);
		client.setQueueName(queueName);

		client.setUserName(user);
		client.setPassword(pass);
		try {
			client.sendMessage(json);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
