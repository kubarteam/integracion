package com.api.rest;


import java.io.StringReader;
import java.util.List;

import javax.ejb.EJB;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.dto.HabitacionDTO;
import com.dto.HotelDTO;
import com.dto.OfertaDTO;
import com.ejb.HotelBeanLocal;

@Path("/Hoteles")
public class HotelesApi {

	@EJB
	private HotelBeanLocal hotelBeanLocal;

	/**
	 * @return the hotelBeanLocal
	 */
	public HotelBeanLocal getHotelBeanLocal() {
		return hotelBeanLocal;
	}

	/**
	 * @param hotelBeanLocal
	 *            the hotelBeanLocal to set
	 */
	public void setHotelBeanLocal(HotelBeanLocal hotelBeanLocal) {
		this.hotelBeanLocal = hotelBeanLocal;
	}

	@POST
	@Path("/ResponseSolicitud")
	@Consumes(MediaType.TEXT_PLAIN)
	public Response ResponseSolicitud(String solicitud) {
		JsonReader reader = Json.createReader(new StringReader(solicitud));
		JsonObject jsonO = reader.readObject();
		reader.close();
		String id= jsonO.getString("ID_Solicitud");
		String estado = jsonO.getString("Estado_Solicitud");
		estado= estado.toUpperCase();
		
		if (id.contains("G"))
			id=id.substring(4, 12);
		
		
			hotelBeanLocal.HotelAprobado(Integer.parseInt(id), estado);
		
		return Response.status(Response.Status.OK).entity("Mensaje Recibido").build();
	}

	@GET
	@Path("/GetHabitacionesHotel")
	@Produces(MediaType.APPLICATION_JSON)
	public List<HabitacionDTO> GetHabitacionesHotel(String id){
		
		return hotelBeanLocal.GetHabitacionesHotel(Integer.parseInt(id));
	}
	
	@POST
	@Path("/PublicarHotel")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String PublicarHotel(String id){
		id = id.substring(3,id.length());
		if(hotelBeanLocal.PublicarHotel(Integer.parseInt(id)))
			return "OK";
		else return "ERROR";
				
	}
	
	public int NuevoHotel(HotelDTO hot){
		return hotelBeanLocal.NuevoHotel(hot);
	}
	
	public int NuevaHabitacion(HabitacionDTO hab){
		return hotelBeanLocal.AgregarHabitacion(hab);
	}
	
	public int NuevaOferta(OfertaDTO ofer){
		return hotelBeanLocal.NuevaOferta(ofer);
	}
	
	
	
	
	@GET
	@Path("/PruebaNuevaOferta")
	public  Response PruebaNuevaOferta(){
		
OfertaDTO ofer= new OfertaDTO();
		
		ofer.setCupo(10);
		ofer.setDesde("22/12/2017");
		ofer.setHasta("30/12/2017");
		ofer.setEstado("ACTIVA");
		ofer.setIdHabitacion(1);
		ofer.setIdHotel(1);
		ofer.setTitulo("la oferta del ano");
		ofer.setPrecio((float)100.5);
		
		int idOferta =NuevaOferta(ofer);
		
		
		return Response.status(Response.Status.OK).entity("Mensaje Recibido").build();	
	}
	
	
	
	
	
	@GET
	@Path("/PruebaNuevoHotel")
	public  Response PruebaNuevoHotel(){
		HotelDTO hotel = new HotelDTO();
		hotel.setDescripcion("Un hotel muy bonito");
		hotel.setDestino("Buenos Aires");
		hotel.setDireccion("Medrano 100");
		hotel.setNombre("Hotel medialuna");
		hotel.setIdsMedioPago(new int[]{1,2,3});
		hotel.setIdsServicios(new int[]{1,2});
		hotel.setUrlFoto("http://DESKTOP-UI6V6UM:8080/oferta-hotelera-web/FotosHoteles/Hoteles-foco.jpg");
		hotel.setUrlUbicacion("http://DESKTOP-UI6V6UM:8080/oferta-hotelera-web/FotosHoteles/ubicacion.jpg");
		hotel.setPoliticasCancelacion("Estas son las politicas");
		

		int idHotel=NuevoHotel(hotel);
		
		HabitacionDTO hab = new HabitacionDTO();
		hab.setDescripcion("Habitacion muy bonita");
		hab.setIdsServicios(new int[]{1,2});
		hab.setUrlFoto("http://DESKTOP-UI6V6UM:8080/oferta-hotelera-web/FotosHoteles/Hoteles-foco.jpg");
		hab.setTipoHabitacion("CUADRUPLE");
		hab.setIdHotel(idHotel);
		
		int idhabitacion=NuevaHabitacion(hab);
		return Response.status(Response.Status.OK).entity("Mensaje Recibido").build();
		
	}
	
	
	
	
	
}