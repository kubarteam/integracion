package com.api.rest;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jms.ConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.Queue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/PruebaColas")
public class PreSubscriptionApiController {
	
	private static final String JMS_QUEUE_PRE_SUBSCRIPTION_QUEUE = "java:/jms/queue/HotelesQueueMDB";
	@Resource(mappedName = JMS_QUEUE_PRE_SUBSCRIPTION_QUEUE)
    private Queue hotelesMDBQueue;
	@Resource(lookup = "java:jboss/DefaultJMSConnectionFactory")
	private static ConnectionFactory connectionFactory;

	public PreSubscriptionApiController() {
		System.out.println("me cree");
	}
	
	/**
	 * @return the hotelesMDBQueue
	 */
	public Queue getHotelesMDBQueue() {
		return hotelesMDBQueue;
	}

	/**
	 * @param hotelesMDBQueue the hotelesMDBQueue to set
	 */
	public void setHotelesMDBQueue(Queue hotelesMDBQueue) {
		this.hotelesMDBQueue = hotelesMDBQueue;
	}
	
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response guardarHotel(@PathParam("id") String id) {
		System.out.println("hotel with id: " + id);
		try (JMSContext context = connectionFactory.createContext();) {
		int count = 0;
	    for (int i = 0; i < 5; i++) { 
	        String message = "This is message " + (i + 1) 
	                + " from producer";
	        // Comment out the following line to send many messages
	        System.out.println("Sending message: " + message); 
	        context.createProducer().send(hotelesMDBQueue, message);
	        count += 1;
	    }
	    System.out.println("Text messages sent: " + count);
	    context.createProducer().send(hotelesMDBQueue, context.createMessage());
		} catch (Exception e) {
		    System.err.println("Exception occurred: " + e.toString());
		    System.exit(1);
		}
		return Response.status(200).entity("OK").build();
	}
}
