package com.api.rest;

import java.io.StringReader;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

import javax.ejb.EJB;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.dto.ServiciosDTO;
import com.ejb.ServiciosBeanLocal;

import vo.LogVO;

@Path("/Servicios")
public class ServiciosApi {

	@EJB
	private ServiciosBeanLocal serviciosBeanLocal;

	/**
	 * @return the serviciosBeanLocal
	 */
	public ServiciosBeanLocal getServiciosBeanLocal() {
		return serviciosBeanLocal;
	}

	/**
	 * @param serviciosBeanLocal
	 *            the serviciosBeanLocal to set
	 */
	public void setServiciosBeanLocal(ServiciosBeanLocal serviciosBeanLocal) {
		this.serviciosBeanLocal = serviciosBeanLocal;
	}

	@POST
	@Path("/CambiarIPBackOffice")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String CambiarIPBack(String ip) {
		JsonReader reader = Json.createReader(new StringReader(ip));
		JsonObject jsonO = reader.readObject();
		reader.close();
		String ipJson = jsonO.getString("ip");
		if (serviciosBeanLocal.CambiarIPBack(ipJson))
			return "OK";
		else
			return "ERROR";
	}

	@POST
	@Path("/CambiarIPPortalWeb")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String CambiarIPPortal(String ip) {
		JsonReader reader = Json.createReader(new StringReader(ip));
		JsonObject jsonO = reader.readObject();
		reader.close();
		String ipJson = jsonO.getString("ip");
		if (serviciosBeanLocal.CambiarIPPortal(ipJson))
			return "OK";
		else
			return "ERROR";
	}

	@POST
	@Path("/CambiarIPPropia")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String CambiarIPPropia(String ip) {
		JsonReader reader = Json.createReader(new StringReader(ip));
		JsonObject jsonO = reader.readObject();
		reader.close();
		String ipJson = jsonO.getString("ip");

		if (serviciosBeanLocal.CambiarIPPropia(ipJson))
			return "OK";
		else
			return "ERROR";
	}

	@POST
	@Path("/NuevosServicios")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response NuevosServicios(List<ServiciosDTO> servicios) {

		serviciosBeanLocal.NuevosServicios(servicios);

		return Response.status(Response.Status.OK).entity("Mensaje Recibido").build();
	}

	@GET
	@Path("/ImportarServicios")
	public Response ImportarServicios() {

		serviciosBeanLocal.ImportarServicios();

		return Response.status(Response.Status.OK).entity("Mensaje Recibido").build();
	}
	
	@GET
	@Path("/TestMensajeria")
	public Response TestMensajeria(){
		Context context;
		try {
			String urlBack="http-remoting://192.168.0.101:8080";
		    final Properties env = new Properties();
            env.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
            env.put(Context.PROVIDER_URL,urlBack);
//            env.put(Context.PROVIDER_URL, System.getProperty(Context.PROVIDER_URL, "http-remoting://192.168.0.101:8080"));
            env.put(Context.SECURITY_PRINCIPAL,  "backofficemensajeria");
            env.put(Context.SECURITY_CREDENTIALS, "mensajeria");
            context = new InitialContext(env);
 
            String connectionFactoryString = System.getProperty("connection.factory", "jms/RemoteConnectionFactory");
            ConnectionFactory connectionFactory = (ConnectionFactory) context.lookup(connectionFactoryString);
 
            String destinationString = System.getProperty("destination", "java:/jms/queue/LogBackOffice");
            Destination destination = (Destination) context.lookup(destinationString);
 
            Connection connection = connectionFactory.createConnection(System.getProperty("username", "backofficemensajeria"), System.getProperty("password", "mensajeria"));
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            //consumer = session.createConsumer(destination);
            connection.start();
		
			MessageProducer producer = session.createProducer(destination);
			
			ObjectMessage message = session.createObjectMessage();
			LogVO pedido = new LogVO();
			pedido.setAccion("Hola");
			pedido.setModulo("Oferta Hotelera");
			pedido.setFecha(Calendar.getInstance().getTime());
			message.setObject(pedido);
			producer.send(message);
			// TODO: recordar cerrar la session y la connection en un bloque �finally�
			connection.close();
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response.status(Response.Status.OK).entity("Mensaje Recibido").build();
	}
}
